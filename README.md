
# KibsReports
A tool to help you simplify the creation and printing of reports from a java programme. Create the visual parts of your reports and receipts the same way you create GUIs using Swing.

At the moment this tool uses 6 basic components.
- Document (The container of all components) 
- Table (Used to display list data)
- TableRow (Used to distribute components accros the page)
- TableColumn (Used to distribute static content across the page)
- Paragraph (Actual container for Text)
- Line (Used to draw lines)
## Usage
Download the tool's KibsReports.jar file from the project root and add it as a dependance in your programme.

#### Create a document object
  ```sh
Document doc = new Document();
doc.setFont(Font.MONOSPACED);
doc.setMarginPercents(7); //margins are set as percentages.
```  

Since version 1.01 you can set properties of an element via method chaining as you see above.
You can configure properties at either page level or at each individual component level.


#### Create a paragraph
  ```sh
Paragraph p = new Paragraph();
p.setData("Hallo World");
p.setFontSize(11);
p.setAlignment(Paragraph.ALIGN_CENTER);
  ```

Or 

  ```sh
Paragraph p = Paragraph()
                .setData("Hallo World")
                .setFontSize(11)
                .setAlignment(Paragraph.ALIGN_CENTER);
  ```

- Again you have the liberty to configure properties at either page or any component level.
- Text data on the paragraph object is set via the setData("text") method.

#### Add our paragraph object to the document object
  ```sh
  doc.addComponent(p);
  ```

#### Print
  ```sh
        try {
            document.print();
        } catch (PrinterException ex) {
            System.err.println(ex.getCause());
        }
```
That simple the file will be printed by your default printer.
Of course you add as many paragraph objects as you wish to the page object.
Each paragraph object spans the width of your page.

## Full demo class


```
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports.demo;

import com.katwekibs.kibsreports.BaseTableModel;
import com.katwekibs.kibsreports.Document;
import com.katwekibs.kibsreports.MyComponent;
import static com.katwekibs.kibsreports.MyComponent.BORDER_ON;
import com.katwekibs.kibsreports.Paragraph;
import static com.katwekibs.kibsreports.Paragraph.Paragraph;
import com.katwekibs.kibsreports.Table;
import com.katwekibs.kibsreports.TableColumn;
import static com.katwekibs.kibsreports.TableColumn.TableColumn;
import java.awt.Font;
import java.awt.print.PrinterException;
import java.util.ArrayList;

/**
 *
 * @author mo
 */
public class PrinterDemo {

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrinterDemo();
            }
        });
    }

    public PrinterDemo() {
        Document document = new Document();
        document.setPageSize(Document.PAGE_SIZE_A4)
                .setPadding(20);

        TableColumn tr = TableColumn()
                .setWidthPercentage(100)
                .setBorderTop(BORDER_ON)
                .setBorderBotom(BORDER_ON)
                .setBottomMargin(20)
                //.setBorder(true)
                .setBorderLineDouble(true)
                .setPadding(20)
                //.setPaddingTop(20)
                //.setPaddingBottom(20)
                .addComponent(
                        Paragraph("Verohs Internet Cafe")
                                .setFontSize(14)
                                .setFontWeight(Paragraph.SYTLE_BOLD)
                                .setWidthPercentage(100)
                )
                .addComponent(Paragraph("123 JohannesMeyer drive"));

        document.setHeader(tr);

        ArrayList testObjs = new ArrayList();
        for (int i = 0; i < 10; i++) {
            testObjs.add(new TestOj("Coll 1 Item " + i, "Coll 2 Item " + i, "Coll 2 Item " + i));
        }

        BaseTableModel baseTableModel = new Model();
        baseTableModel.setData(testObjs);

        Table table = new Table()
                .setFont(Font.MONOSPACED)
                .setPadding(10);
        //.setLeftMargin(10);
        table.getColumnHeader().setBorderTop(Table.BORDER_ON);
        table.getColumnHeader().setBorderBotom(Table.BORDER_ON);
        //table.getColumnHeader().setFont(Font.MONOSPACED);
        table.getColumnHeader().setFontWeight(MyComponent.SYTLE_BOLD);
        table.getColumnHeader().setPadding(3).setBottomMargin(10);
        table.setModel(baseTableModel);
        table.setBorderBotom(1);
        table.setBottomMargin(30);
        table.getBorder().setDrawDaubleLine(true);

        table.getColumnHeader().getComponentAt(1).setAlignment(Table.ALIGN_CENTER);
        table.getColumnHeader().getComponentAt(2).setAlignment(Table.ALIGN_RIGHT);
        document.addComponent(table);

        document.addComponent(Paragraph(
                "Make sure your savings cover the deposit as well as the transfer and "
                + "bond registration costs of the property, including all fees "
                + "charged by the conveyancer and attorneys. If in doubt, ask the "
                + "home loan consultant at the bank or a bond originator to calculate "
                + "how much you will be granted, to ascertain whether you can afford the "
                + "monthly instalments 3. Ensure you also look at the costs that will be "
                + "incurred once you purchase the property – for example the monthly costs "
                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
                + " costs, water and electricity deposit, your telephone and internet/WiFi "
                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
                + "often forget to take these costs into account when calculating their monthly "
                + "costs, forgetting these need to be included too. ").setBottomMargin(10).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        document.addComponent(Paragraph(
                "Make sure your savings cover the deposit as well as the transfer and "
                + "bond registration costs of the property, including all fees "
                + "charged by the conveyancer and attorneys. If in doubt, ask the "
                + "home loan consultant at the bank or a bond originator to calculate "
                + "how much you will be granted, to ascertain whether you can afford the "
                + "monthly instalments 3. Ensure you also look at the costs that will be "
                + "incurred once you purchase the property – for example the monthly costs "
                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
                + " costs, water and electricity deposit, your telephone and internet/WiFi "
                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
                + "often forget to take these costs into account when calculating their monthly "
                + "costs, forgetting these need to be included too. ").setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        document.addComponent(Paragraph(
                "Make sure your savings cover the deposit as well as the transfer and "
                + "bond registration costs of the property, including all fees "
                + "charged by the conveyancer and attorneys. If in doubt, ask the "
                + "home loan consultant at the bank or a bond originator to calculate "
                + "how much you will be granted, to ascertain whether you can afford the "
                + "monthly instalments 3. Ensure you also look at the costs that will be "
                + "incurred once you purchase the property – for example the monthly costs "
                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
                + " costs, water and electricity deposit, your telephone and internet/WiFi "
                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
                + "often forget to take these costs into account when calculating their monthly "
                + "costs, forgetting these need to be included too. ").setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        document.addComponent(Paragraph(
                "Make sure your savings cover the deposit as well as the transfer and "
                + "bond registration costs of the property, including all fees "
                + "charged by the conveyancer and attorneys. If in doubt, ask the "
                + "home loan consultant at the bank or a bond originator to calculate "
                + "how much you will be granted, to ascertain whether you can afford the "
                + "monthly instalments 3. Ensure you also look at the costs that will be "
                + "incurred once you purchase the property – for example the monthly costs "
                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
                + " costs, water and electricity deposit, your telephone and internet/WiFi "
                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
                + "often forget to take these costs into account when calculating their monthly "
                + "costs, forgetting these need to be included too. ").setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        document.addComponent(Paragraph(
                "Make sure your savings cover the deposit as well as the transfer and "
                + "bond registration costs of the property, including all fees "
                + "charged by the conveyancer and attorneys. If in doubt, ask the "
                + "home loan consultant at the bank or a bond originator to calculate "
                + "how much you will be granted, to ascertain whether you can afford the "
                + "monthly instalments 3. Ensure you also look at the costs that will be "
                + "incurred once you purchase the property – for example the monthly costs "
                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
                + " costs, water and electricity deposit, your telephone and internet/WiFi "
                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
                + "often forget to take these costs into account when calculating their monthly "
                + "costs, forgetting these need to be included too. ").setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        document.addComponent(Paragraph(
                "Make sure your savings cover the deposit as well as the transfer and "
                + "bond registration costs of the property, including all fees "
                + "charged by the conveyancer and attorneys. If in doubt, ask the "
                + "home loan consultant at the bank or a bond originator to calculate "
                + "how much you will be granted, to ascertain whether you can afford the "
                + "monthly instalments 3. Ensure you also look at the costs that will be "
                + "incurred once you purchase the property – for example the monthly costs "
                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
                + " costs, water and electricity deposit, your telephone and internet/WiFi "
                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
                + "often forget to take these costs into account when calculating their monthly "
                + "costs, forgetting these need to be included too. ").setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        document.addComponent(Paragraph(
                "Make sure your savings cover the deposit as well as the transfer and "
                + "bond registration costs of the property, including all fees "
                + "charged by the conveyancer and attorneys. If in doubt, ask the "
                + "home loan consultant at the bank or a bond originator to calculate "
                + "how much you will be granted, to ascertain whether you can afford the "
                + "monthly instalments 3. Ensure you also look at the costs that will be "
                + "incurred once you purchase the property – for example the monthly costs "
                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
                + " costs, water and electricity deposit, your telephone and internet/WiFi "
                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
                + "often forget to take these costs into account when calculating their monthly "
                + "costs, forgetting these need to be included too. ").setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        document.addComponent(Paragraph("Header 2"));
        document.addComponent(Paragraph("Line 2").setBottomMargin(300));
        document.addComponent(Paragraph("Line 2").setBottomMargin(300));
        document.addComponent(Paragraph("Header 3"));
        document.addComponent(Paragraph("Line 3").setBottomMargin(300));
        document.addComponent(Paragraph("Line 3").setBottomMargin(300));
        document.addComponent(Paragraph("Header 4"));
        document.addComponent(Paragraph("Line 4").setBottomMargin(300).setBorder(true));
        document.addComponent(Paragraph("Line 4").setBorder(true));
        document.setBorder(true);
        try {
            document.print();
        } catch (PrinterException ex) {
            System.err.println(ex.getCause());
        }
    }

    public class Model extends BaseTableModel {

        private final String[] columnNames = {"col 1", "col 2", "col 3"};

        public Model() {
            super();
            this.setColumnNames(columnNames);
        }

        @Override
        public Object getValueAt(int row, int col) {
            TestOj object = (TestOj) this.getObjectAt(row);
            Object res = null;
            switch (col) {
                case 0:
                    res = object.getCol1();
                    break;
                case 1:
                    res = object.getCol2();
                    break;
                case 2:
                    res = object.getCol3();
                    break;
            }
            return res;
        }
    }

    public class TestOj {

        private String col1;
        private String col2;
        private String col3;

        public TestOj(String col1, String col2, String col3) {
            this.col1 = col1;
            this.col2 = col2;
            this.col3 = col3;
        }

        public String getCol1() {
            return col1;
        }

        public void setCol1(String col1) {
            this.col1 = col1;
        }

        public String getCol2() {
            return col2;
        }

        public void setCol2(String col2) {
            this.col2 = col2;
        }

        public String getCol3() {
            return col3;
        }

        public void setCol3(String col3) {
            this.col3 = col3;
        }

    }
}

```