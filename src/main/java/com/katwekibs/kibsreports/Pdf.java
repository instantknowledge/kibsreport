/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;
import org.apache.pdfbox.pdmodel.PDDocument;

/**
 *
 * @author muhamedkakembo
 */
public class Pdf extends MyComponent<Pdf> {
   private float boxWidth = 20;
   private float boxHeight = 20;

    public Pdf() {
        setMarginPercents(0);
    }

    public Pdf(PDDocument inputPDF) {
        setData(inputPDF);
        setMarginPercents(0);
    }

    public static Pdf Pdf(PDDocument inputPDF) {
        return new Pdf().setData(inputPDF);
    }

     /**
     * sets the width of the component's max allowed drawing width the total imeagable width of the component;
     * @param width 
     */
    public Pdf setWidthActual(float width) {
        super.setWidth(width); //To change body of generated methods, choose Tools | Templates.
        return this;
    }
    

    
    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
                drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    public float getBoxWidth() {
        return boxWidth;
    }

    public Pdf setBoxWidth(float boxWidth) {
        this.boxWidth = boxWidth;
        return this;
    }

    public float getBoxHeight() {
        return boxHeight;
    }

    public Pdf setBoxHeight(float height) {
        this.boxHeight = height;
        return this;
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        
        renderer.drawPdf(this);
        
        afterDrawing(renderer);
        return getLength();
    }
    
    
}
