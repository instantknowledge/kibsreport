/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.print.PrintService;

/**
 *
 * @author muhamedkakembo
 */
public abstract class PrinterObj {

    protected String printerName;
    protected PageFormat pageFormat;
    protected PrinterJob job;

    public PrinterObj() {
        job = PrinterJob.getPrinterJob();
        pageFormat = job.defaultPage();
        setPrinterJob(job);
    }

    public PageFormat getPageFormat() {
        return pageFormat;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public PrinterJob getJob() {
        return job;
    }

    public void setJob(PrinterJob job) {
        this.job = job;
    }

    public abstract void performPrint(Printable doc);

    public void performPrint() throws PrinterException {
        performPrint(false);
    }

    public void performPrint(boolean showPrintDialog) throws PrinterException {
        if (showPrintDialog) {
            if (job.printDialog()) {
                print();
            }
        } else {
            print();
        }

    }

    private void print() throws PrinterException {
        try {
            job.print();
        } catch (PrinterException e) {
            throw e;
        }
    }

    public abstract PrintService getPrintService();

    protected final void setPrinterJob(PrinterJob job) {
        PrintService printer = getPrintService();
        if (printer != null) {
            try {
                job.setPrintService(printer);
            } catch (PrinterException ex) {
                System.err.println("could not connect to a printer " + printer.getName());
            }
        }
    }
}
