/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;

/**
 *
 * @author muhamedkakembo
 */
public class Document implements Pageable, Printable {

    private final ArrayList<MyComponent> components = new ArrayList<>();
    private final ArrayList<MyComponent> componentsQuae = new ArrayList<>();
    private final ArrayList<MyComponent> headers = new ArrayList<>();
    /**
     * @deprecated use headers
     */
    private MyComponent header;
    private MyComponent footer;
    private boolean hasBorder = false;
    private boolean showPageNumbers = true;
    private float pageWidth = 595.276f;
    private float pageHight = 841.89f;
    private boolean emptyPrint = false;
    private boolean showPrintDialog = true;
    private String oriantation = ORIANTATION_POTRAIT;

    private ArrayList<Page> pages;

    public static final String PAGE_SIZE_A6 = "a6";
    public static final String PAGE_SIZE_A5 = "a5";
    public static final String PAGE_SIZE_A4 = "a4";
    public static final String PAGE_SIZE_A3 = "a3";
    public static final String PAGE_SIZE_CUSTOM = "custom";

    public static final String ORIANTATION_POTRAIT = "1";
    public static final String ORIANTATION_LANDSCAPE = "2";
    /**
     * infinit page height.But you are required to provide page width.
     */
    public static final String PAGE_SIZE_ROLL = "roll";

    private String pageSize = PAGE_SIZE_A4;
    private Page page;
    private final ReportPrinter printer;
    private PageFormat pageFormat;
    private String name = "_";

    public Document(String name) {
        this.name = name;
        printer = new ReportPrinter();
        pageFormat = (PageFormat) printer.getPageFormat();
        page = new Page(this);
    }

    public String getOriantation() {
        return oriantation;
    }

    public Document setOriantation(String oriantation) {
        this.oriantation = oriantation;

        if (oriantation.equals(ORIANTATION_LANDSCAPE)) {
            pageFormat.setOrientation(PageFormat.LANDSCAPE);
        }
        return this;
    }

    public void setName(String docName) {
        this.name = docName;
    }

    public String getName() {
        return name;
    }

    public static File createTempFile(String prefix, String suffix) {
        File parent = new File(System.getProperty("java.io.tmpdir"));

        File temp = new File(parent, prefix + suffix);

        if (temp.exists()) {
            temp.delete();
        }

        try {
            temp.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return temp;
    }

    public void print() throws PrinterException {
        renderForPrint();
        printer.getJob().setPageable(this);
        printer.performPrint(isShowPrintDialog());
    }

    public Document printToScreen() {
        renderForPrint();
        PrintPreview.ShowPreview(this);
        return this;
    }

    /**
     * Prints a pdf to a temp file and returns the file path;
     *
     * @return
     */
    public File printToPdf() {
        File file = createTempFile(getName(), ".pdf");
        printToPdf(file);
        return file;
    }

    public void printToPdf(File file) {

        renderForPrint();

        try {
            PDDocument doc = new PDDocument();
            for (int i = 0; i < getPages().size(); i++) {
                Page page = getPages().get(i);
                page.setIndex(i + 1);
                page.setTotalNumPages(getPages().size());
                page.renderPDF(doc);
                page.freeResources();
            }
            doc.save(file);
            doc.close();
        } catch (IOException ex) {
            Logger.getLogger(Document.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void renderForPrint() {
        // only run this method if pages == to null;
        if (getPages() != null) {
            return;
        }

        //determin the page size
        setPageMeasurement();

        //load all components to doc comp quea
        setCoponentQuea();

        //create a paper object and set it to the sizes we got above.
        Paper paper = new Paper();
        paper.setSize(getPageWidth(), getPageHeight());
        paper.setImageableArea(0, 0, getPageWidth(), getPageHeight());

        //if roll printing, set height to whatever printer gives us.
        if (isRollPrint()) {
            setShowPageNumbers(false);
            setShowPrintDialog(false);
            paper.setSize(getPageWidth(), pageFormat.getImageableHeight());
            paper.setImageableArea(0, 0, getPageWidth(), pageFormat.getImageableHeight());
        }

        //Are we performing empty print like for roll printer just to kick drawer open?
        if (isEmptyPrint()) {
            paper.setSize(216, 0.1);
            paper.setImageableArea(0, 0, 1, 1);
        }

        pageFormat.setPaper(paper);
        int pageIndex = -1;
        pages = new ArrayList<>();

        do {
            pageIndex++;
            createPage(pageIndex);
        } while (componentsQuae.iterator().hasNext() && !isEmptyPrint());

/////we are removing printing page numbers like this to adding page footer
//        if (isShowPageNumbers()) {
//            for (int i = 0; i < pages.size(); i++) {
//                Page page = pages.get(i);
//                page.drawPageNumber(i, pages.size());
//                page.freeResources();
//            }
//        }
    }

    private void createPage(int pageIndex) {
        Page p = new Page(this);
        //set padding
        p.setPaddingTop(page.getPaddingTop());
        p.setPaddingLeft(page.getPaddingLeft());
        p.setPaddingBottom(page.getPaddingBottom());
        p.setPaddingRight(page.getPaddingRight());

        //set font size
        p.setFontSize(page.getFontSize());

        //set margin
        p.setTopMargin(page.getMarginTop());
        p.setRightMargin(page.getMarginRight());
        p.setBottomMargin(page.getMarginBottom());
        p.setLeftMargin(page.getMarginLeft());

//        //set border and fill
        p.setBorder(page.isHasBorder());
        p.setFill(page.hasAFill());
        p.setFillColor(page.getFillColor());
        p.setBorderColor(page.getBorderColor());
        p.setBackGroundImage(page.getBackGroundImage());

        p.setIndex(pageIndex + 1);
        pages.add(pageIndex, p);
        p.setComponents(componentsQuae);
        p.renderG2D();
    }

    public boolean isEmptyPrint() {
        return emptyPrint;
    }

    public void setEmptyPrint(boolean emptyPrint) {
        this.emptyPrint = !emptyPrint;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Document addComponent(MyComponent component) {
        components.add(component);
        return this;
    }

    /**
     * @deprecated use getHeaders()
     * @return
     */
    public MyComponent getHeader() {
        getHeaders().get(0);
        return header;
    }

    public ArrayList<MyComponent> getHeaders() {
        return headers;
    }

    public Document setHeader(MyComponent header) {
        addHeader(header);
        return this;
    }

    public Document addHeader(MyComponent header) {
        this.headers.add(header);
        return this;
    }

    public MyComponent getFooter() {
        return footer;
    }

    public Document setFooter(MyComponent footer) {
        this.footer = footer;
        return this;
    }

    public Document setPageSize(String pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public Document setPadding(float padding) {
        getPage().setPaddingBottom(padding).setPaddingLeft(padding).setPaddingRight(padding).setPaddingTop(padding);
        return this;
    }

    public Document setMargins(int margins) {
        getPage().setMargins(margins);
        return this;
    }

    public Document setTopMargin(int margins) {
        getPage().setTopMargin(margins);
        return this;
    }

    public Document setFontSize(int fontSize) {
        getPage().setFontSize(fontSize);
        return this;
    }

    /**
     * Set custom page width in mm
     *
     * @param width
     * @param height
     */
    public Document setCustomPageSize(float width, float height) {
        this.pageWidth = width * 2.835f;
        this.pageHight = height * 2.835f;
        return this;
    }

    public Document setBorder(boolean hasBorder) {
        getPage().setBorder(hasBorder);
        return this;
    }

    public Document setBackGroungImage(BufferedImage backGroung) {
        getPage().setBackGroundImage(backGroung);
        return this;
    }

    public Document setFill(boolean hasBorder) {
        getPage().setFill(hasBorder);
        return this;
    }

    public Document setFillColor(Color fillColor) {
        getPage().setFillColor(fillColor);
        return this;
    }

    public boolean isShowPageNumbers() {
        return showPageNumbers;
    }

    public Document setShowPageNumbers(boolean showPageNumbers) {
        this.showPageNumbers = showPageNumbers;
        return this;
    }

    private void setPageMeasurement() {
        switch (getPageSize()) {
            case PAGE_SIZE_A6:
                pageWidth = 297.638f;
                pageHight = 419.528f;
                break;
            case PAGE_SIZE_A5:
                pageWidth = 419.528f;
                pageHight = 595.276f;
                break;
            case PAGE_SIZE_A4:
                pageWidth = 595.276f;
                pageHight = 841.89f;
                break;
            case PAGE_SIZE_A3:
                pageWidth = 841.89f;
                pageHight = 1190.551f;
                break;
            case PAGE_SIZE_ROLL:
                pageHight = 0;
            case PAGE_SIZE_CUSTOM:
                break;

        }
    }

    private String getPageSize() {
        return pageSize;
    }

    public float getPageWidth() {
        return pageWidth;
    }

    private void setShowPrintDialog(boolean showPrintDialog) {
        this.showPrintDialog = showPrintDialog;
    }

    public float getPageHeight() {
        return pageHight;
    }

    protected ArrayList<MyComponent> getComponents() {
        return components;
    }

    public Document setPageHeight(float pageHeight) {
        this.pageHight = pageHeight;
        return this;
    }

    /**
     * @deprecated use setPageWidith(float pageWidth);
     * @param pageWidth
     * @return
     */
    public Document setPageWidith(int pageWidth) {
        this.pageWidth = pageWidth;
        return this;
    }

    public Document setPageWidith(float pageWidth) {
        this.pageWidth = pageWidth;
        return this;
    }

    boolean isRollPrint() {
        return pageSize.equals(PAGE_SIZE_ROLL);
    }

    boolean isShowPrintDialog() {
        return this.showPrintDialog;
    }

    private void setCoponentQuea() {
        for (int i = 0; i < getComponents().size(); i++) {
            MyComponent component = getComponents().get(i);
            componentsQuae.add(component);
        }
    }

    public ArrayList<Page> getPages() {
        return pages;
    }

    public void setPages(ArrayList<Page> pages) {
        this.pages = pages;
    }

    public PageFormat getPageFormat() {
        return pageFormat;
    }

    @Override
    public int getNumberOfPages() {
        return getPages().size();
    }

    @Override
    public PageFormat getPageFormat(int pageIndex) throws IndexOutOfBoundsException {
        return printer.getPageFormat();
    }

    @Override
    public Printable getPrintable(int pageIndex) throws IndexOutOfBoundsException {
        return this;
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        int totalNumPages = getPages().size();
        if (isRollPrint()) {
            totalNumPages = 1;
        }

        Graphics2D g2 = (Graphics2D) graphics;
        if (pages.size() > pageIndex) {

            Page p = pages.get(pageIndex);
            p.setGraphic2d(g2);
            p.renderG2D();
            //p.drawPageNumber(pageIndex, totalNumPages);
//
//            //graphic2d.translate(0f, (-pageIndex * getImageableHeight()));
            //g2.translate(0f, -pageIndex * p.getLength());
            //p.freeResources();
            //g2.drawImage(p.getCanvas(), 0, 0, null);
        } else {
            return NO_SUCH_PAGE;
        }

        return PAGE_EXISTS;
    }

}
