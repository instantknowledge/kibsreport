/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;

/**
 *
 * @author muhamedkakembo
 */
public class CheckBox extends MyComponent<CheckBox> {

    private float boxWidth = 20;
    private float boxHeight = 20;

    public CheckBox() {
        setMarginPercents(0);
    }

    public CheckBox(String data) {
        setData(data);
        setMarginPercents(0);
    }

    public static CheckBox CheckBox(String data) {
        return new CheckBox().setData(data);
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    public float getBoxWidth() {
        return boxWidth;
    }

    /**
     * sets a box width of of a check box;
     * @param boxWidth
     * @return 
     */
    public CheckBox setBoxWidth(float boxWidth) {
        this.boxWidth = boxWidth;
        return this;
    }

    /**
     * sets the width of the component's max allowed drawing width the total imeagable width of the component;
     * @param width 
     */
    public CheckBox setWidthActual(float width) {
        super.setWidth(width); //To change body of generated methods, choose Tools | Templates.
        return this;
    }
    

    public float getBoxHeight() {
        return boxHeight;
    }

    public CheckBox setBoxHeight(float height) {
        this.boxHeight = height;
        return this;
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);

        renderer.drawCheckBox(this);

        afterDrawing(renderer);
        return getLength();
    }

}
