/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;

/**
 *
 * @author muhamedkakembo
 */
public class VShape extends MyComponent<VShape> {

   
    public static final String  DIRECTION_RIGHT  = "right";
    public static final String DIRECTION_LEFT  = "left";
    public static final String  DIRECTION_BOTTOM_RIGHT  = "bottomRight";
    public static final String DIRECTION_BOTTOM_LEFT  = "bottomLeft";

     private float boxHeight;
    private float boxWidth;
    private String vDirection = DIRECTION_LEFT;
    
    public VShape() {
        setMarginPercents(0);
    }

    public VShape(String data) {
        setData(data);
        setMarginPercents(0);
    }

    public static VShape VShape(String data) {
        return new VShape().setData(data);
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);

        renderer.drawVShape(this);

        afterDrawing(renderer);
        return getLength();
    }

    public float getBoxWidth() {
        return boxWidth;
    }

    public VShape setBoxWidth(float boxWidth) {
        this.boxWidth = boxWidth;
        setWidth(boxWidth);
        return this;
    }

    public float getBoxHeight() {
        return boxHeight;
    }

    public VShape setBoxHeight(float height) {
        this.boxHeight = height;
        return this;
    }

    public String getvDirection() {
        return vDirection;
    }

    public VShape setvDirection(String vDirection) {
        this.vDirection = vDirection;
        return this;
    }
    

}
