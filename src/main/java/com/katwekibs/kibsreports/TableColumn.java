/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;

/**
 *
 * @author muhamedkakembo
 */
public class TableColumn extends MyComponent<TableColumn> {

    public TableColumn() {
        setMarginPercents(0);
    }

    public static TableColumn TableColumn() {
        return new TableColumn();
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);

        if (this.getComponents().size() > 0) {
            for (int i = 0; i < this.getComponents().size(); i++) {
                MyComponent cell = this.getComponents().get(i);
                cell.drawData(renderer, this.getImageableX(), this.getImageableY(), this.getImageableWidth(), this.getImageableHeight());

                translateHeight(cell.getLength());
            }

        }

        afterDrawing(renderer);
        return this.getLength();
    }

}
