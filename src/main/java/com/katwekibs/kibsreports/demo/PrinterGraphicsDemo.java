package com.katwekibs.kibsreports.demo;

import com.katwekibs.kibsreports.BaseTableModel;
import static com.katwekibs.kibsreports.CheckBox.CheckBox;
import com.katwekibs.kibsreports.Document;
import com.katwekibs.kibsreports.MyComponent;
import static com.katwekibs.kibsreports.MyComponent.BORDER_ON;
import com.katwekibs.kibsreports.Paragraph;
import static com.katwekibs.kibsreports.Paragraph.Paragraph;
import com.katwekibs.kibsreports.Table;
import com.katwekibs.kibsreports.TableColumn;
import static com.katwekibs.kibsreports.TableColumn.TableColumn;
import com.katwekibs.kibsreports.TableRow;

import java.awt.Font;
import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrinterGraphicsDemo {

    public PrinterGraphicsDemo() {
        print();
    }

    public void print() {

        Document document = new Document("Demo Doc");
        document.setPageSize(Document.PAGE_SIZE_A4)
                .setPadding(20)
                .setBorder(true);

        TableRow trs = new TableRow();
        document.addComponent(trs);

        trs.addComponent(
                CheckBox("hello ") //this is a test stance for setting a marifing in teh doh thlos thodo
                        .setMargin(10)
                        .setRightMargin(5)
                        .setWidthPercentage(20)
                        .setBoxHeight(15)
                        .setBoxWidth(15)
                        .setFont(Font.MONOSPACED)
                        .setFontStyle(BORDER_ON));
        trs.addComponent(
                CheckBox("hello Warning") //this is a test stance for setting a marifing in teh doh thlos thodo
                        .setMargin(10)
                        .setRightMargin(5)
                        .setWidthPercentage(20)
                        .setBoxHeight(15)
                        .setBoxWidth(15)
                        .setFont(Font.MONOSPACED)
                        .setFontStyle(BORDER_ON));
//
        TableColumn tr = TableColumn()
                .setWidthPercentage(100)
                .setBorderTop(BORDER_ON)
                .setBorderBotom(BORDER_ON)
                .setBottomMargin(50)
                //.setBorder(true)
                .setBorderLineDouble(true)
                .setPadding(20)
                //.setPaddingTop(20)
                //.setPaddingBottom(20)
                .addComponent(
                        Paragraph("Verohs Internet Cafe")
                                .setFontSize(14)
                                .setFontWeight(Paragraph.SYTLE_BOLD)
                                .setWidthPercentage(100)
                )
                .addComponent(Paragraph("123 JohannesMeyer drive"));

        document.setHeader(tr);

        ArrayList testObjs = new ArrayList();
        for (int i = 0; i < 80; i++) {
            testObjs.add(new TestOj("Coll 1 Item " + i, "Coll 2 Item " + i, "Coll 2 Item " + i));
        }

        BaseTableModel baseTableModel = new Model();
        baseTableModel.setData(testObjs);

        Table table = new Table()
                .setFont(Font.MONOSPACED)
                .setPadding(10);
        //.setLeftMargin(10);
        table.getColumnHeader().setBorderTop(Table.BORDER_ON);
        table.getColumnHeader().setBorderBotom(Table.BORDER_ON);
        table.getColumnHeader().setFont(Font.MONOSPACED);
        table.getColumnHeader().setFontWeight(MyComponent.SYTLE_BOLD);
        table.getColumnHeader().setPadding(3).setBottomMargin(10);
        table.setModel(baseTableModel);
        //table.setBorderBotom(1);
        //table.setBottomMargin(30);
        table.getBorder().setDrawDaubleLine(true);
        table.setBorder(true);

        table.getColumnHeader().getComponentAt(1).setAlignment(Table.ALIGN_CENTER);
        table.getColumnHeader().getComponentAt(2).setAlignment(Table.ALIGN_RIGHT);
        document.addComponent(table);
//
//        document.addComponent(Paragraph(
//                "P 1 Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//               // .setBottomMargin(10)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true));
//        document.addComponent(Paragraph(
//                " P 1 Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                " P 2 Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(600)
//                .setBorder(true)
//        );
//        document.addComponent(Paragraph(
//                " p 3 Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ")
//                .setBottomMargin(2)
//                .setPaddingLeft(20)
//                .setPaddingRight(20)
//                .setPaddingTop(20)
//                .setPaddingBottom(20)
//                .setBorder(true)
//                .setPageBrake(true)
//        );
//
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ").setPageBrake(true).setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ").setPageBrake(true).setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ").setPageBrake(true).setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ").setPageBrake(true).setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
//        document.addComponent(Paragraph(
//                "Make sure your savings cover the deposit as well as the transfer and "
//                + "bond registration costs of the property, including all fees "
//                + "charged by the conveyancer and attorneys. If in doubt, ask the "
//                + "home loan consultant at the bank or a bond originator to calculate "
//                + "how much you will be granted, to ascertain whether you can afford the "
//                + "monthly instalments 3. Ensure you also look at the costs that will be "
//                + "incurred once you purchase the property – for example the monthly costs "
//                + "such as rates and taxes and levies, as well as homeowner’s insurance, removal"
//                + " costs, water and electricity deposit, your telephone and internet/WiFi "
//                + "connection fee, as well as satellite tv dish set-up and connection. Buyers "
//                + "often forget to take these costs into account when calculating their monthly "
//                + "costs, forgetting these need to be included too. ").setPageBrake(true).setBottomMargin(2).setPaddingLeft(20).setPaddingRight(20).setPaddingTop(20).setPaddingBottom(20).setBorder(true));
        try {
            //document.printToScreen();
            document.print();
        } catch (PrinterException ex) {
            Logger.getLogger(PrinterGraphicsDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {

        new PrinterGraphicsDemo();

    }

    public class Model extends BaseTableModel {

        private final String[] columnNames = {"col 1", "col 2", "col 3"};

        public Model() {
            super();
            this.setColumnNames(columnNames);
        }

        @Override
        public Object getValueAt(int row, int col) {
            TestOj object = (TestOj) this.getObjectAt(row);
            Object res = null;
            switch (col) {
                case 0:
                    res = object.getCol1();
                    break;
                case 1:
                    res = object.getCol2();
                    break;
                case 2:
                    res = object.getCol3();
                    break;
            }
            return res;
        }
    }

    public class TestOj {

        private String col1;
        private String col2;
        private String col3;

        public TestOj(String col1, String col2, String col3) {
            this.col1 = col1;
            this.col2 = col2;
            this.col3 = col3;
        }

        public String getCol1() {
            return col1;
        }

        public void setCol1(String col1) {
            this.col1 = col1;
        }

        public String getCol2() {
            return col2;
        }

        public void setCol2(String col2) {
            this.col2 = col2;
        }

        public String getCol3() {
            return col3;
        }

        public void setCol3(String col3) {
            this.col3 = col3;
        }

    }

}
