/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports.strategies.renderStrategy;

import com.katwekibs.kibsreports.MyComponent;

/**
 *
 * @author mo
 */
public interface Renderer {
    abstract void drawLine(MyComponent.Border border);
    abstract void drawText(MyComponent component);
    abstract void drawCheckBox(MyComponent component);
    abstract void drawBorder(MyComponent.Border border);
                abstract void drawBackGroundImage(MyComponent component);
    abstract void setClipRectangle(MyComponent component);
    abstract void clearRect();
    abstract void dispose();

    abstract void drawImage(MyComponent component);
    abstract void drawRectangle(MyComponent component);

    public void drawCircle(MyComponent component);
    public void drawVShape(MyComponent c);
    public void drawPdf(MyComponent component);
}
