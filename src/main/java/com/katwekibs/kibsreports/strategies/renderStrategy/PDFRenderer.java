/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports.strategies.renderStrategy;

import com.katwekibs.kibsreports.CheckBox;
import com.katwekibs.kibsreports.Circle;
import com.katwekibs.kibsreports.Image;
import com.katwekibs.kibsreports.VShape;
import com.katwekibs.kibsreports.MyComponent;
import com.katwekibs.kibsreports.Paragraph;
import static com.katwekibs.kibsreports.Paragraph.Paragraph;
import com.katwekibs.kibsreports.Pdf;
import com.katwekibs.kibsreports.Rectangle;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.multipdf.LayerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceCMYK;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;
import rst.pdfbox.layout.text.Alignment;
import rst.pdfbox.layout.text.Position;
import rst.pdfbox.layout.text.TextFlow;

/**
 *
 * @author mo
 */
public class PDFRenderer implements Renderer {

    private int saveCounter;
    private final MyComponent.PDFCanvas pdfCanvas;

    public PDFRenderer(MyComponent.PDFCanvas pdfCanvas) {
        this.pdfCanvas = pdfCanvas;
    }

    @Override
    public void drawLine(MyComponent.Border border) {
        try {

            PDPageContentStream contents = pdfCanvas.getPageContentStream();
            contents.setStrokingColor(border.getColor());
            contents.moveTo(border.getX(), border.getY());
            contents.lineTo(border.getWIDTH() + border.getX(), border.getY());
            contents.stroke();
            if (border.isDrawDaubleLine()) {

                float name = border.getDoubleLineStrut();
                contents.moveTo(border.getX(), border.getY() + name);
                contents.lineTo(border.getWIDTH() + border.getX(), border.getY() + name);
                contents.stroke();
            }
            contents.setStrokingColor(Color.BLACK);
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void drawText(MyComponent component) {
        try {
            PDPageContentStream contentStream = pdfCanvas.getPageContentStream();

            TextFlow text = new TextFlow();
            text.addText((String) component.getData(), component.getFontSize(), returnFontStyle(component, PDType1Font.HELVETICA));
            text.setMaxWidth(component.getImageableWidth());

            // Calculate vertical alignment if component height > 0
            //Adjust Y position for vertical alignment
            component.setVerticleOffset(text.getHeight());
            
    
            if (component.isDrawingCall()) {
                text.drawText(contentStream, new Position(component.getImageableX(), component.getImageableY()), getPdfTextAlignment(component), null);
            }

            // Shift Y coordinate to the height of this line we just drawn
            component.shiftY(text.getHeight());

            // Add this line's height to the component length
            component.increaseLength(text.getHeight());
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private PDType1Font returnFontStyle(MyComponent component, PDType1Font font) {
        switch (component.getFontStyle()) {
            case MyComponent.SYTLE_BOLD:
                if (font.equals(PDType1Font.HELVETICA)) {
                    return PDType1Font.HELVETICA_BOLD;
                } else if (font.equals(PDType1Font.TIMES_ROMAN)) {
                    return PDType1Font.TIMES_BOLD;
                } else if (font.equals(PDType1Font.COURIER)) {
                    return PDType1Font.COURIER_BOLD;
                }
                break;
            case MyComponent.SYTLE_ITALIC:
                if (font.equals(PDType1Font.HELVETICA)) {
                    return PDType1Font.HELVETICA_OBLIQUE;
                } else if (font.equals(PDType1Font.TIMES_ROMAN)) {
                    return PDType1Font.TIMES_ITALIC;
                } else if (font.equals(PDType1Font.COURIER)) {
                    return PDType1Font.COURIER_OBLIQUE;
                }
                break;
            case MyComponent.SYTLE_BOLD_ITALIC:
                if (font.equals(PDType1Font.HELVETICA)) {
                    return PDType1Font.HELVETICA_BOLD_OBLIQUE;
                } else if (font.equals(PDType1Font.TIMES_ROMAN)) {
                    return PDType1Font.TIMES_BOLD_ITALIC;
                } else if (font.equals(PDType1Font.COURIER)) {
                    return PDType1Font.COURIER_BOLD_OBLIQUE;
                }
                break;
            default:
                break;
        }
        return font;
    }

    private Alignment getPdfTextAlignment(MyComponent c) {
        switch (c.getAlignment()) {
            case Paragraph.ALIGN_JUSTIFIED:
                return Alignment.Justify;
            case Paragraph.ALIGN_CENTER:
                return Alignment.Center;
            case Paragraph.ALIGN_RIGHT:
                return Alignment.Right;
            default:
                return Alignment.Left;
        }
    }

    @Override
    public void drawBorder(MyComponent.Border border) {
        try {
            float offset = border.getDoubleLineStrut(); // adjust this to get the desired distance between the two lines
            float innerWidth = border.getWIDTH() - 2 * offset;
            float innerHeight = border.getHEIGHT() - 2 * offset;

            //drawfill
            if (border.hasAfill()) {
                if (border.isDrawDaubleLine()) {
                    pdfCanvas.getPageContentStream().setNonStrokingColor(border.getFillColor());
                    pdfCanvas.getPageContentStream().addRect(border.getX(), border.getY() - border.getHEIGHT(), border.getWIDTH(), border.getHEIGHT());
                    pdfCanvas.getPageContentStream().fill();

                } else {
                    pdfCanvas.getPageContentStream().setNonStrokingColor(border.getFillColor());
                    pdfCanvas.getPageContentStream().addRect(border.getX(), border.getY() - border.getHEIGHT(), border.getWIDTH(), border.getHEIGHT());
                    pdfCanvas.getPageContentStream().fill();
                }
            }

            // Draw outer rectangle
            if (border.canDrawBoarder()) {
                pdfCanvas.getPageContentStream().setLineWidth(border.getLineDepth());
                pdfCanvas.getPageContentStream().addRect(border.getX(), border.getY() - border.getHEIGHT(), border.getWIDTH(), border.getHEIGHT());
                pdfCanvas.getPageContentStream().stroke();
                // Draw inner rectangle
                if (border.isDrawDaubleLine()) {
                    pdfCanvas.getPageContentStream().setLineWidth(border.getLine2Depth());
                    pdfCanvas.getPageContentStream().addRect(border.getX() + offset, (border.getY() - border.getHEIGHT()) + offset, innerWidth, innerHeight);
                    pdfCanvas.getPageContentStream().stroke();
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void drawBackGroundImage(MyComponent component) {
        try {
            //draw Backgroung
            if (component.hasBackGround()) {

                BufferedImage backgroundImage = component.getBackGroundImage();
//            float imageWidth = backgroundImage.getWidth();
//            float imageHeight = backgroundImage.getHeight();
//            float scaleX = border.getWIDTH() / imageWidth;
//            float scaleY = border.getHEIGHT() / imageHeight;

                // Draw the background image
                PDImageXObject pdImage = LosslessFactory.createFromImage(pdfCanvas.getPdfDoc(), backgroundImage);
                PDPageContentStream contentStream = pdfCanvas.getPageContentStream();
                contentStream.drawImage(pdImage, component.getImageableX(), component.getImageableY() - component.getImageableHeight(), component.getImageableWidth(), component.getImageableHeight());
            }
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void setClipRectangle(MyComponent component) {
        try {
            clearRect();
            contentStreamSaveState(pdfCanvas);

            pdfCanvas.getPageContentStream().addRect(
                    component.getImageableX() - 1,
                    component.getImageableY() - component.getImageableHeight() + 1,
                    component.getImageableWidth() + 2,
                    component.getImageableHeight()
            );

            pdfCanvas.getPageContentStream().clip();

        } catch (IOException e) {
            // throwException(e);
        }
    }

    @Override
    public void clearRect() {
        if (saveCounter == 0) {
            return;
        }
        saveCounter--;
        try {
            pdfCanvas.getPageContentStream().restoreGraphicsState();
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void contentStreamSaveState(MyComponent.PDFCanvas pdfCanvas) throws IOException {
        saveCounter++;
        pdfCanvas.getPageContentStream().saveGraphicsState();
    }

    @Override
    public void dispose() {
        pdfCanvas.dispose();
    }

    @Override
    public void drawCheckBox(MyComponent c) {
        try {
            CheckBox component = (CheckBox) c;

            // Get or set the line width. For example, let's assume 1 unit.
            float lineWidth = 1.0f;
            pdfCanvas.getPageContentStream().setLineWidth(lineWidth);

            // Adjust the coordinates for the line width
            float adjustedX = component.getImageableX() + (lineWidth / 2);
            float adjustedY = component.getImageableY() - component.getBoxHeight() + (lineWidth / 2);
            float adjustedWidth = component.getBoxWidth() - lineWidth;
            float adjustedHeight = component.getBoxHeight() - lineWidth;

            pdfCanvas.getPageContentStream().addRect(adjustedX, adjustedY, adjustedWidth, adjustedHeight);

            if (component.isDrawingCall()) {
                pdfCanvas.getPageContentStream().stroke();
            }
            float boxWidth = component.getBoxWidth();
            float marginRight = component.getMarginRight();

            component.translateWidth(boxWidth + marginRight);
            //component.increaseLength((int) component.getBoxHeight());

            Paragraph p = Paragraph((String) component.getData());
            p.setFont(component.getFont().getFontName());
            p.setFontSize(component.getFontSize());
            p.setFontWeight(component.getFontWeight());
            p.setFontStyle(component.getFontStyle());
            p.setDrawingCall(component.isDrawingCall());
            p.beforeDrawing(this, component.getImageableX(), component.getImageableY(), component.getImageableWidth(), component.getImageableHeight());
            drawText(p);
            p.afterDrawing(this);

            float length = component.getBoxHeight();
            if (p.getLength() > length) {
                length += p.getLength() - component.getBoxHeight();
            }
            component.translateHeight(length);
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void drawImage(MyComponent c) {
        Image component = (Image) c;

        BufferedImage img = (BufferedImage) component.getData();
        try {
            PDImageXObject pdImage = LosslessFactory.createFromImage(pdfCanvas.getPdfDoc(), img);
            PDPageContentStream contentStream = pdfCanvas.getPageContentStream();

            // Draw the image at full size at coordinates (x=20, y=20).
            if (component.isDrawingCall()) {
                component.translateHeight((int) component.getBoxHeight());
                contentStream.drawImage(pdImage, component.getImageableX(), component.getImageableY(), component.getBoxWidth(), component.getBoxHeight());
            }

            //component.translateHeight((int) component.getBoxHeight());
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void drawRectangle(MyComponent c) {
        try {
            Rectangle component = (Rectangle) c;
            PDPageContentStream contentStream = pdfCanvas.getPageContentStream();

            contentStream.setNonStrokingColor(component.getFillColor());

            if (component.isDrawingCall()) {
                contentStream.addRect(component.getImageableX(), component.getImageableY() - component.getBoxHeight(), component.getBoxWidth(), component.getBoxHeight());
                contentStream.fill();
            }
            contentStream.restoreGraphicsState();
            component.translateHeight(component.getBoxHeight());
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void drawCircle(MyComponent c) {
        try {

            Circle component = (Circle) c;
            PDPageContentStream contentStream = pdfCanvas.getPageContentStream();
            contentStream.saveGraphicsState();

            float x = component.getImageableX();
            float y = component.getImageableY();
            float width = component.getBoxWidth();
            float height = component.getBoxHeight();

            float radius = width / 2;
            // Calculate the center of the circle based on the top-left corner
            float centerX = x + radius;
            float centerY = y - radius;

            if (component.isDrawingCall()) {
                // Set color to black using CMYK
                PDColor blackColorCMYK = new PDColor(new float[]{0, 0, 0, 1}, PDDeviceCMYK.INSTANCE);
                contentStream.setNonStrokingColor(blackColorCMYK);

                // Draw the circle using curveTo (Bezier curves)
                contentStream.moveTo(centerX + radius, centerY);
                contentStream.curveTo(centerX + radius, centerY + radius,
                        centerX, centerY + radius,
                        centerX, centerY + radius);
                contentStream.curveTo(centerX - radius, centerY + radius,
                        centerX - radius, centerY,
                        centerX - radius, centerY);
                contentStream.curveTo(centerX - radius, centerY - radius,
                        centerX, centerY - radius,
                        centerX, centerY - radius);
                contentStream.curveTo(centerX + radius, centerY - radius,
                        centerX + radius, centerY,
                        centerX + radius, centerY);

                contentStream.fill();  // Fill the circle
            }
            contentStream.restoreGraphicsState();
            component.translateHeight(width);
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void drawVShape(MyComponent c) {
        VShape component = (VShape) c;

        try {
            PDPageContentStream contents = pdfCanvas.getPageContentStream();
            contents.setStrokingColor(Color.BLACK);
            float lineWidth = 1.0f; // Set this to the desired value or retrieve it from the component if needed
            contents.setLineWidth(lineWidth);

            float offset = lineWidth / 2;

            switch (component.getvDirection()) {
                case VShape.DIRECTION_LEFT:
                    contents.moveTo(component.getImageableX() + offset, component.getImageableY() + 12.75f - offset);
                    contents.lineTo(component.getImageableX() + offset, component.getImageableY() + offset);
                    contents.moveTo(component.getImageableX() + 12.75f - offset, component.getImageableY() + (12.75f / 2));
                    break;
                case VShape.DIRECTION_RIGHT:
                    contents.moveTo(component.getImageableX() - offset, component.getImageableY() + 12.75f - offset);
                    contents.lineTo(component.getImageableX() - offset, component.getImageableY() + offset);
                    contents.moveTo(component.getImageableX() - 12.75f + offset, component.getImageableY() + (12.75f / 2));
                    break;
                case VShape.DIRECTION_BOTTOM_LEFT:
                    contents.moveTo(component.getImageableX() + offset, component.getImageableY() - 12.75f + offset);
                    contents.lineTo(component.getImageableX() + offset, component.getImageableY() - offset);
                    contents.moveTo(component.getImageableX() + 12.75f - offset, component.getImageableY() - (12.75f / 2));
                    break;
                case VShape.DIRECTION_BOTTOM_RIGHT:
                    contents.moveTo(component.getImageableX() - offset, component.getImageableY() - 12.75f + offset);
                    contents.lineTo(component.getImageableX() - offset, component.getImageableY() - offset);
                    contents.moveTo(component.getImageableX() - 12.75f + offset, component.getImageableY() - (12.75f / 2));
                    break;
            }

            contents.lineTo(component.getImageableX(), component.getImageableY());
            contents.stroke();

            contents.setStrokingColor(Color.BLACK);
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void drawPdf(MyComponent c) {
        try {
            Pdf component = (Pdf) c;
            PDDocument inputPDF = (PDDocument) component.getData();
            PDPage inputPage = inputPDF.getPage(0);
            LayerUtility layerUtility = new LayerUtility(pdfCanvas.getPdfDoc());
            PDFormXObject formXObject = layerUtility.importPageAsForm(inputPDF, 0);

            PDPageContentStream contentStream = pdfCanvas.getPageContentStream();
            contentStream.saveGraphicsState();
            contentStream.transform(Matrix.getTranslateInstance(component.getImageableX(), component.getImageableY() - inputPage.getMediaBox().getHeight()));
            contentStream.drawForm(formXObject);
            contentStream.restoreGraphicsState();
            component.translateHeight(inputPage.getMediaBox().getHeight());
        } catch (IOException ex) {
            Logger.getLogger(PDFRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
