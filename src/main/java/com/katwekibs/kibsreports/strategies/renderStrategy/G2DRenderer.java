/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports.strategies.renderStrategy;

import com.katwekibs.kibsreports.CheckBox;
import com.katwekibs.kibsreports.Image;
import com.katwekibs.kibsreports.MyComponent;
import com.katwekibs.kibsreports.MyComponent.Border;
import com.katwekibs.kibsreports.MyComponent.DoubleBoarderType;
import com.katwekibs.kibsreports.Paragraph;
import static com.katwekibs.kibsreports.Paragraph.Paragraph;
import com.katwekibs.kibsreports.Rectangle;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;

/**
 *
 * @author mo
 */
public class G2DRenderer implements Renderer {

    private final Graphics2D canvas;

    public G2DRenderer(Graphics2D canvas) {
        this.canvas = canvas;
        this.canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    @Override
    public void drawLine(Border border) {
        Paint oldPaint = canvas.getPaint();
        canvas.setPaint(border.getColor());
        canvas.setStroke(border.getStroke());
        canvas.drawLine((int) border.getX(), (int) border.getY(), (int) border.getWIDTH() + (int) border.getX(), (int) border.getY());
        if (border.isDrawDaubleLine()) {
            float strut = border.getDoubleLineStrut();
            canvas.drawLine((int) border.getX(), (int) border.getY() + (int) strut, (int) border.getWIDTH() + (int) border.getX(), (int) border.getY() + (int) strut);
        }
        canvas.setPaint(oldPaint);
    }

//    @Override
//    public void drawText(MyComponent component) {
//        String name = (String) component.getData();
//        if (name.isEmpty()) {
//            name = " ";
//        }
//        AttributedString as = new AttributedString(name);
//        as.addAttribute(TextAttribute.FONT, component.getFont());
//
//        performParagraphSettings(as, component);
//
//        AttributedCharacterIterator aci = as.getIterator();
//        FontRenderContext frc = canvas.getFontRenderContext();
//        LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);
//
//        float lineHeigt = 0;
//        while (lbm.getPosition() < aci.getEndIndex()) {
//            float leftMargin = 0;
//            leftMargin = component.getImageableX();
//            TextLayout textLayout = lbm.nextLayout(component.getImageableWidth());
//            float textWidth = (float) textLayout.getVisibleAdvance();
//            switch (component.getAlignment()) {
//                case Paragraph.ALIGN_JUSTIFIED:
//                    textLayout = textLayout.getJustifiedLayout(component.getImageableWidth());
//                    break;
//                case Paragraph.ALIGN_CENTER:
//                    leftMargin = component.alignCenter(leftMargin, textWidth);
//                    break;
//                case Paragraph.ALIGN_RIGHT:
//                    leftMargin = component.alignRight(leftMargin, textWidth);
//                    break;
//            }
//
//            lineHeigt = textLayout.getAscent() + textLayout.getDescent() + textLayout.getLeading();
//            if (component.isDrawingCall()) {
//                textLayout.draw(canvas, leftMargin, component.getImageableY() + textLayout.getAscent());
//            }
//
//            //shift Y cordinate to the height of this line we just drawn.
//            component.shiftY(lineHeigt);
//
//            //Add this line's height to the component length.
//            component.increaseLength(lineHeigt);
//        }
//        //if component has a bottom line. add a single line height to component length.
//        //alse shift our Y cordinate.
//        if (component.isBottomLine()) {
//            component.shiftY(lineHeigt);
//            component.increaseLength(lineHeigt);
//        }
//    }
    public void drawText(MyComponent component) {
        String name = (String) component.getData();
        if (name.isEmpty()) {
            name = " ";
        }

        // Split the text by newline characters
        String[] lines = name.split("\n");

        float lineHeight = 0;

        for (String line : lines) {
            // Create an AttributedString for each line
            AttributedString as = new AttributedString(line);
            as.addAttribute(TextAttribute.FONT, component.getFont());

            performParagraphSettings(as, component);

            AttributedCharacterIterator aci = as.getIterator();
            FontRenderContext frc = canvas.getFontRenderContext();
            LineBreakMeasurer lbm = new LineBreakMeasurer(aci, frc);

            while (lbm.getPosition() < aci.getEndIndex()) {
                float leftMargin = component.getImageableX();
                TextLayout textLayout = lbm.nextLayout(component.getImageableWidth());
                float textWidth = (float) textLayout.getVisibleAdvance();

                switch (component.getAlignment()) {
                    case Paragraph.ALIGN_JUSTIFIED:
                        textLayout = textLayout.getJustifiedLayout(component.getImageableWidth());
                        break;
                    case Paragraph.ALIGN_CENTER:
                        leftMargin = component.alignCenter(leftMargin, textWidth);
                        break;
                    case Paragraph.ALIGN_RIGHT:
                        leftMargin = component.alignRight(leftMargin, textWidth);
                        break;
                }

                lineHeight = textLayout.getAscent() + textLayout.getDescent() + textLayout.getLeading();
                
                // Calculate vertical alignment if component height > 0
                component.setVerticleOffset(lineHeight);
                
                if (component.isDrawingCall()) {
                    textLayout.draw(canvas, leftMargin, component.getImageableY());
                }

                // Shift Y coordinate to the height of this line we just drew.
                component.shiftY(lineHeight);

                // Add this line's height to the component length
                component.increaseLength(lineHeight);
            }

            // After each line, shift the Y position for the newline spacing
            component.shiftY(lineHeight);  // This moves down for each new line, preserving the behavior
        }

        // Handle bottom line if needed
        if (component.isBottomLine()) {
            component.shiftY(lineHeight);
            component.increaseLength(lineHeight);
        }
    }

    private void performParagraphSettings(AttributedString as, MyComponent c) {
        if (c.getFontWeight() == MyComponent.SYTLE_BOLD) {
            as.addAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_EXTRABOLD);
        }

        if (c.getFontStyle() == MyComponent.SYTLE_ITALIC) {
            Font font = c.getFont();
            Font newFont = new Font(font.getName(), Font.ITALIC, c.getFontSize());
            as.addAttribute(TextAttribute.FONT, newFont);
        }

        if (c.getUnderLine() == MyComponent.SYTLE_UNDERLINE) {
            as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        }
    }

    @Override
    public void drawBorder(Border border) {

        //save old canvas settings
        Stroke oldStroke = canvas.getStroke();
        Paint oldPaint = canvas.getPaint();

        float thickness = border.getDoubleLineStrut();  // set the thickness of the double line

        //drawfill
        if (border.hasAfill()) {
             canvas.setPaint(border.getFillColor());
                canvas.fillRect((int) border.getX(), (int) border.getY(), (int) (border.getWIDTH()), (int) (border.getHEIGHT()));
                canvas.setPaint(oldPaint);

        }
        
        // Draw outer rectangle
        if (border.canDrawBoarder()) {

            // draw the outer rectangle
            canvas.setPaint(border.getColor());
            canvas.setStroke(border.getStroke(DoubleBoarderType.OUTER));
            canvas.drawRect((int) border.getX(), (int) border.getY(), (int) border.getWIDTH(), (int) border.getHEIGHT());
            canvas.setPaint(oldPaint);
            // draw the inner rectangle
            if (border.isDrawDaubleLine()) {

                canvas.setPaint(border.getColor());
                canvas.setStroke(border.getStroke(DoubleBoarderType.INNER));
                canvas.drawRect((int) border.getX() + (int) thickness, (int) border.getY() + (int) thickness, (int) border.getWIDTH() - 2 * (int) thickness, (int) border.getHEIGHT() - 2 * (int) thickness);
                canvas.setPaint(oldPaint);
            }
        }
        

        //reset old settings 
        canvas.setPaint(oldPaint);
        canvas.setStroke(oldStroke);
    }

    public void setClipRectangle(MyComponent component) {
        Rectangle2D.Float rect = new Rectangle2D.Float(
                component.getImageableX() - 2,
                component.getImageableY() - 2,
                component.getImageableWidth() + 1,
                component.getImageableHeight());
        canvas.setClip(rect);
    }

    public void clearRect() {
        canvas.setClip(null);
    }

    @Override
    public void dispose() {
        canvas.dispose();
    }

    @Override
    public void drawCheckBox(MyComponent c) {
        CheckBox component = (CheckBox) c;

        canvas.setColor(Color.BLACK);
        if (component.isDrawingCall()) {
            canvas.drawRect((int) component.getImageableX(), (int) component.getImageableY(), (int) component.getBoxWidth(), (int) component.getBoxHeight());
        }

        //shift Y cordinate to the height of this line we just drawn.
        component.translateWidth((int) component.getBoxWidth() + component.getMarginRight());
        //component.translateHeight((int) component.getBoxHeight());

        Paragraph p = Paragraph((String) component.getData());
        p.setFont(component.getFont().getFontName());
        p.setFontSize(component.getFontSize());
        p.setFontWeight(component.getFontWeight());
        p.setFontStyle(component.getFontStyle());
        p.setDrawingCall(component.isDrawingCall());
        p.beforeDrawing(this, component.getImageableX(), component.getImageableY(), component.getImageableWidth(), component.getImageableHeight());
        drawText(p);
        p.afterDrawing(this);

        float length = component.getLength();
        if (p.getLength() > length) {
            length += p.getLength() - (int) component.getBoxHeight();
        }
        component.translateHeight(length);
    }

    @Override
    public void drawImage(MyComponent c) {
        Image component = (Image) c;

        BufferedImage img = (BufferedImage) component.getData();
        if (component.isDrawingCall()) {
            canvas.drawImage(img, (int) component.getImageableX(), (int) component.getImageableY(), (int) component.getBoxWidth(), (int) component.getBoxHeight(), null);
        }

        //shift Y cordinate to the height of this line we just drawn.
        //component.translateWidth((int) component.getBoxWidth() + component.getMarginRight());
        component.translateHeight((int) component.getBoxHeight());
    }

    @Override
    public void drawRectangle(MyComponent c) {
        Rectangle component = (Rectangle) c;

        if (component.getFillColor() != null) {
            canvas.setColor(component.getFillColor());
            if (component.isDrawingCall()) {
                canvas.fillRect((int) component.getImageableX(), (int) component.getImageableY(), (int) component.getBoxWidth(), (int) component.getBoxHeight());
            }
        } else {
            if (component.isDrawingCall()) {
                canvas.drawRect((int) component.getImageableX(), (int) component.getImageableY(), (int) component.getBoxWidth(), (int) component.getBoxHeight());
            }
        }
        canvas.setColor(Color.BLACK);
        component.translateHeight(component.getLength());
    }

    @Override
    public void drawBackGroundImage(MyComponent component) {
    }

    @Override
    public void drawCircle(MyComponent component) {
    }

    @Override
    public void drawVShape(MyComponent c) {
    }

    @Override
    public void drawPdf(MyComponent component) {

    }
}
