/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports.strategies.pageOriginStrategy;

/**
 *
 * @author mo
 */
public class BottomLeftOrigin extends OriginStrategy{


    @Override
    public float shiftImageableY(float imageableY, float newValue) {
        return imageableY - newValue;
    }

    @Override
    public float setImageableY(float imageableY, float imageableHeight) {
        if(imageableY == 0){
            imageableY = imageableHeight;
        }
        return imageableY;
    }

    public float getPageBottomY(float pageHeight, float bottomMargin){
        return bottomMargin;
        
    }

    @Override
    public float imageableYandOffset(float componentLength, float componentYoffset) {
        return componentLength + componentYoffset;
    }
   
    
}
