/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports.strategies.pageOriginStrategy;

/**
 *
 * @author mo
 */
public abstract class OriginStrategy {

    public static OriginStrategy TopLeftOrigin = new TopLeftOrigin();
    public static OriginStrategy BottomLeftOrigin = new BottomLeftOrigin();

    public abstract float setImageableY(float imageableY,float imageableHeight);

    public abstract float shiftImageableY(float imageableY, float newValue);
    
    public abstract float getPageBottomY(float pageHeight, float bottomMargin);
    public abstract float imageableYandOffset(float input1, float input2);

}
