/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;

/**
 *
 * @author muhamedkakembo
 */
public class TableRow extends MyComponent<TableRow> {

    public TableRow() {
        setMarginPercents(0);
    }

    public static TableRow TableRow() {
        return new TableRow();
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        
         //get old drawing call
            boolean oldDrawingCall = isDrawingCall();
            
        //measure row and set height of its tallest cell to all cells
        float rowHeight = drawData2(false, renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        rowHeight = rowHeight - (getMarginTop() + getMarginBottom());
        setMaxRowHeightToAllComponents(rowHeight);

        //now draw the row.
        drawData2(oldDrawingCall, renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    private float getMax(float[] args) {
        float max = args[0];
        for (float value : args) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }

    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        setDrawingCall(drawingCall);
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        if (this.getComponents().size() > 0) {
            float[] imageableYs = new float[getComponents().size()];
            for (int i = 0; i < this.getComponents().size(); i++) {
                MyComponent cell = this.getComponents().get(i);
                cell.drawData(renderer, this.getImageableX(), this.getImageableY(), this.getImageableWidth(), this.getImageableHeight());

                if(isDrawingCall()){
                this.shiftX(cell.getWidth() + cell.getMarginRight() + cell.getMarginLeft());
                }
                imageableYs[i] = cell.getLength();

            }
            translateHeight(getMax(imageableYs));
        }
        afterDrawing(renderer);
        return this.getLength();
    }

    private void setMaxRowHeightToAllComponents(float rowHeight) {
        for (int i = 0; i < this.getComponents().size(); i++) {
            MyComponent cell = this.getComponents().get(i);
            cell.setHeight(rowHeight);
        }
    }
}
