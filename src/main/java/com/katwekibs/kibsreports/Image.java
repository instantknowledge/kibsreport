/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;
import java.awt.image.BufferedImage;

/**
 *
 * @author muhamedkakembo
 */
public class Image extends MyComponent<Image> {
   private float boxWidth = 20;
   private float boxHeight = 20;

    public Image() {
        setMarginPercents(0);
    }

    public Image(BufferedImage bufferedImage) {
        setData(bufferedImage);
        setMarginPercents(0);
    }

    public static Image Image(BufferedImage bufferedImage) {
        return new Image().setData(bufferedImage);
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
                drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    public float getBoxWidth() {
        return boxWidth;
    }

    public Image setBoxWidth(float boxWidth) {
        this.boxWidth = boxWidth;
        return this;
    }

    public float getBoxHeight() {
        return boxHeight;
    }

    public Image setBoxHeight(float height) {
        this.boxHeight = height;
        return this;
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        
        renderer.drawImage(this);
        
        afterDrawing(renderer);
        return getLength();
    }
    
    
}
