/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import static com.katwekibs.kibsreports.Paragraph.Paragraph;
import com.katwekibs.kibsreports.strategies.pageOriginStrategy.OriginStrategy;
import com.katwekibs.kibsreports.strategies.renderStrategy.G2DRenderer;
import com.katwekibs.kibsreports.strategies.renderStrategy.PDFRenderer;
import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

/**
 *
 * @author muhamedkakembo
 */
public class Page extends MyComponent<Page> implements Cloneable {

    private Document document;
    private int totalNumPages;
    private PageFormat pageFormat;
    private Graphics2D graphic2d;
    private BufferedImage canvas;
    private List<MyComponent> cachedComponets = new ArrayList<>();
    private Renderer renderer;
    private int index;

    public Page() {
    }

    @Override
    public Page clone() {
        Page clone = (Page) super.clone();
        clone.resetCachedComponets();
        initConvas();
        return clone;
    }

    public Page(Document document) {
        this.document = document;
        this.pageFormat = document.getPageFormat();

        initConvas();
    }

    public void initConvas() {
        canvas = new BufferedImage((int) pageFormat.getImageableWidth(), (int) pageFormat.getImageableHeight(), BufferedImage.TYPE_INT_ARGB);
        graphic2d = (Graphics2D) canvas.getGraphics();

        graphic2d.setColor(Color.BLACK);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getTotalNumPages() {
        return totalNumPages;
    }

    public void setTotalNumPages(int totalNumPages) {
        this.totalNumPages = totalNumPages;
    }

    public void renderG2D() {

        renderer = new G2DRenderer(graphic2d);
        setUpPage(renderer);
        drawData(renderer, getImageableX(), getImageableY(), getImageableWidth(), getImageableHeight());
    }

    public void renderPDF(PDDocument doc) throws IOException {

        setOrigin(OriginStrategy.BottomLeftOrigin);

        PDPage blankPage = new PDPage(new PDRectangle((float) pageFormat.getWidth(), (float) pageFormat.getHeight()));
        doc.addPage(blankPage);

        renderer = new PDFRenderer(new PDFCanvas(doc, blankPage));
        setUpPage(renderer);
        drawData(renderer, getImageableX(), getImageableY(), getImageableWidth(), getImageableHeight());
    }

    @Override
    public Page setBaseline() {
        super.setBaseline();
        this.baseLine += this.getMarginTop();
        return this;//To change body of generated methods, choose Tools | Templates.
    }

    public static Page Page() {
        return new Page();
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        float pageLength = 0;

        if (hasBackGround()) {
            renderer.drawBackGroundImage(this);
        }

        //Draw page boarder
        if (isHasBorder() || hasAFill() || hasBackGround()) {
            renderer.drawBorder(getBorder());
            translateHeight(getBorder().getLength());

            //top boarder line and bottom boarder line
            pageLength += getBorder().getLength();
        }

        //Draw a header components.
        pageLength += drawPageHeaders(true);

        //backup height information before geting component height
        float imageableYOld = getImageableY();
        float imageableHeightOld = getImageableHeight();

        //get footer height
        float footerHeight = drawPageFooter(false);

        //move cursor to footer position
        translateHeight(getImageableHeight() - footerHeight);

        //Draw footer
        footerHeight = drawPageFooter(true);

        //move cursor back to backed up position.
        
        setImageableY(imageableYOld);
//        increaseLength(footerHeight);
         setImageableHeight(imageableHeightOld - (footerHeight/2));
          //translateHeight(footerHeight-imageableHeightOld);


        pageLength += footerHeight;

        //clip so that left overs from previous page dont draw into the header
        //clip so that left overs from previous page dont draw into the header
        renderer.setClipRectangle(this);

        float clippedHeight = getImageableHeight();

        if (!cachedComponets.isEmpty()) {
            //we are in the drawing phase, we use cached components from measuring phase
            //clear all components.
            components.clear();

            //add components from cache
            for (int i = 0; i < cachedComponets.size(); i++) {
                MyComponent c = cachedComponets.get(i);
                addComponent(c);
            }

            Iterator iterator = getComponents().iterator();
            while (iterator.hasNext()) {
                MyComponent component = (MyComponent) iterator.next();
                float len = drawPageComponent(component, true);
                translateHeight(len);
            }

        } else {
            ListIterator iterator = getComponents().listIterator();
            List<MyComponent> overflowComponents = new ArrayList<>();
            while (iterator.hasNext()) {
                //save the current imageableHeight before we translate it.
                float pageImageableHcached = this.getImageableHeight();

                MyComponent component = (MyComponent) iterator.next();
                //get component height
                float compLength = drawPageComponent(component, false);

                float lengthSofar = pageLength + compLength;
                 if (lengthSofar <= imageableHeight) {
                    // Case 1: Component fits on current page
                    pageLength = performDrawPageComp(component, pageLength, imageableHeight, iterator);
                    cachedComponets.add(component);
                } else if (compLength > clippedHeight) {

                    // Case 2: Component is larger than clippedHeight; draw part, clone rest
                    pageLength = performDrawPageComp(component, pageLength, imageableHeight, iterator);
                                        cachedComponets.add(component);  // Cache the original component

                    //clone to save another piece back
                    MyComponent c = component.clone();
                    if (c != null) {
                        c.setYOffSet(c.getYOffest() + pageImageableHcached);
                        // Store for next page
                        overflowComponents.add(c);
                        break;
                    }
                } else {
                    // Case 3: Component does not fit but is less than clippedHeight
                    overflowComponents.add(component);  // Store for next page
                    break;
                }

                if (pageLength >= imageableHeight) {
                    pageLength = 0;
                    break;
                }
            }

            // Add overflow components to the beginning of `components` for next page processing
getComponents().addAll(0, overflowComponents);
        }
    }

    private float performDrawPageComp(MyComponent component, float pageLength, float imageableHeight1, ListIterator iterator) {
        float compLength = drawPageComponent(component, true);
        translateHeight(compLength);
        pageLength += compLength;
        if (component.isPageBrake()) {
            translateHeight(imageableHeight1 - pageLength);
            pageLength += imageableHeight1 - pageLength;
        }
        iterator.remove();
        return pageLength;
    }

    private float drawPageHeaders(boolean drawingCall) {
        float res = 0;
        for (MyComponent header : document.getHeaders()) {
            int pageIndex = getIndex();
            //is this header restricted to a page?
            //if its restriction list is not empty it is restricted
            if (header.getComponentVisiblePages().size() > 0) {
                //If this page index is not in the list, it should not appear on this page
                if (!header.getComponentVisiblePages().contains(pageIndex)) {
                    continue;
                }
            }
            header.setParent(this);
            header.afterAddEvent();
            header.setChildrenDefaultFontSize(header.getFontSize());

            if (drawingCall) {
                header.setDrawingCall(true);
            } else {
                header.setDrawingCall(false);
            }

            header.drawData(renderer, getImageableX(), getImageableY(), getImageableWidth(), getImageableHeight());
            translateHeight(header.getLength());
            res += header.getLength();
        }
        return res;
    }

    private float drawPageFooter(boolean drawingCall) {
        MyComponent footer = document.getFooter();
        if (footer == null) {
            return 0;
        }
        footer.setParent(this);
        footer.afterAddEvent();
        footer.setDrawingCall(drawingCall);

        footer.setChildrenDefaultFontSize(footer.getFontSize());
        footer.drawData(renderer, getImageableX(), getImageableY(), getImageableWidth(), getImageableHeight());
        return footer.getLength();
    }

    private float drawPageComponent(MyComponent component, boolean drawingCall) {
        if (drawingCall) {
            component.setDrawingCall(true);
        } else {
            component.setDrawingCall(false);
        }

        component.drawData(
                renderer, this.getImageableX(),
                getOrigin().imageableYandOffset(this.getImageableY(), component.getYOffest()),
                this.getImageableWidth(),
                this.getImageableHeight()
        );
        return getOrigin().imageableYandOffset(component.getLength(), component.getYOffest());
    }

    public void renderPages(PageFormat pageFormat) {
        this.pageFormat = pageFormat;

        // No translation in roll print, we only need to print one long page.
        if (document.isRollPrint()) {
            return;
        }
        BufferedImage image = new BufferedImage((int) pageFormat.getImageableWidth(), (int) pageFormat.getImageableHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        Renderer renderer = new G2DRenderer(g2);

        setUpPage(renderer);
        setImageableHeight(getImageableHeight() - getMarginBottom() - getPaddingBottom());

        float imageableHeight1 = getImageableHeight();

        Page renderComponent = renderComponent(renderer, getImageableWidth(), getImageableHeight(), getImageableX(), getImageableY());

        totalNumPages = (int) Math.ceil(((double) renderComponent.getLength()) / imageableHeight1);

    }

    private void setUpPage(Renderer renderer) {

        float pageWidth = (float) pageFormat.getImageableWidth();
        float pageHeight = (float) pageFormat.getImageableHeight();
        float x = (float) pageFormat.getImageableX();
        float y = (float) pageFormat.getImageableY();

        this.setWidth(this.calculatePecentageSize(this.getWidthPercentage(), pageWidth));
        //set component margins
        setMargins();

        //remove margins from width and move pen to position
        setImegeables(x, y, pageWidth, pageHeight);

        //set border origin before removing padding. should we need to draw a border latter.
        setBorderOrigin(getImageableX(), getImageableY(), getImageableWidth(), getImageableHeight() - getMarginBottom());

        setPaddings();
        setImageableHeight(getImageableHeight() - getMarginBottom() - getPaddingBottom());
    }

    public BufferedImage getCanvas() {
        return canvas;
    }

    public void setCanvas(BufferedImage canvas) {
        this.canvas = canvas;
    }

    public Graphics2D getGraphic2d() {
        return graphic2d;
    }

    public void setGraphic2d(Graphics2D graphic2d) {
        this.graphic2d = graphic2d;
    }

    public PageFormat getPageFormat() {
        return pageFormat;
    }

    public void setPageFormat(PageFormat pageFormat) {
        this.pageFormat = pageFormat;
    }

    public void freeResources() {
        if (graphic2d != null) {
            graphic2d.dispose();
        }

        if (renderer != null) {
            renderer.dispose();
        }
    }

    public List<MyComponent> getCachedComponets() {
        return cachedComponets;
    }

    public void setCachedComponets(List<MyComponent> cachedComponets) {
        this.cachedComponets = cachedComponets;
    }

    public void resetCachedComponets() {
        this.cachedComponets.clear();
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
