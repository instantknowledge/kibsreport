/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author muhamedkakembo
 */
public class DynamicParagraph extends Paragraph {

    private String parentComponent;
    private String field;

    public DynamicParagraph() {
        setMarginPercents(0);
    }

    public DynamicParagraph(String parentComponent, String field) {
        this.parentComponent = parentComponent;
        this.field = field;

        setData(getData());
        setMarginPercents(0);

    }

    public static DynamicParagraph DynamicParagraph(String parentComponent, String fieldName) {
        return (DynamicParagraph) new DynamicParagraph(parentComponent, fieldName);
    }

    @Override
    public final Object getData() {
        if (this.data != null) {
            return data;
        }

        int fieldData =-1;
        MyComponent dynamicParent = getDynamicParent(parentComponent);
        if (dynamicParent != null) {
            try {
                Field parentField = dynamicParent.getClass().getDeclaredField(field);
                parentField.setAccessible(true);
                fieldData = (int) parentField.get(dynamicParent);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(DynamicParagraph.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        String res = null;
        if(fieldData > -1){
            res = String.valueOf(fieldData);
        }
        return res;
    }

    public MyComponent getDynamicParent(String className) {
        MyComponent currentParent = this.getParent();

        while (currentParent != null) {
            if (currentParent.getClass().getSimpleName().equals(className)) {
                return currentParent;
            }
            currentParent = currentParent.getParent();
        }

        return null; // Return null if no matching parent was found
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        super.drawData(renderer, imageableX, imageableY, imageableWidth, imageableHeight); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}
