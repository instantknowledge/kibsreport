/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.pageOriginStrategy.OriginStrategy;
import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;

/**
 *
 * @author muhamedkakembo
 */
public abstract class MyComponent<T extends MyComponent> implements Cloneable {

    public static final String DEFAULT_FONT = "Serif";
    public static final int DEFAULT_FONT_SIZE = 10;
    public static final String DEFAULT_ALIGNMENT = "left";
    public static final float DEFAULT_WIDTH_PERCENTAGE = 100;
    public static final float DEFAULT_LEFT_MARGIN_PERCENTAGE = 7;
    public static final float DEFAULT_RGIHT_MARGIN_PERCENTAGE = 7;
    public static final float DEFAULT_BOTTOM_MARGIN_PERCENTAGE = 7;
    public static final float DEFAULT_TOP_MARGIN_PERCENTAGE = 7;
    //
    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_RIGHT = "right";
    public static final String ALIGN_CENTER = "center";
    public static final String ALIGN_JUSTIFIED = "justified";
    public static final String VERTICLE_ALIGN_TOP = "top";
    public static final String VERTICLE_ALIGN_CENTER = "center";
    public static final String VERTICLE_ALIGN_BOTTOM = "bottom";
    public static final int SYTLE_BOLD = 1;
    public static final int SYTLE_ITALIC = 2;
    public static final int SYTLE_BOLD_ITALIC = 3;
    public static final int SYTLE_UNDERLINE = 1;
    public static final int BORDER_ON = 1;
    //
    protected Object data;
    protected int fontSize = 10;
    protected Font font = new Font("Serif", Font.PLAIN, this.fontSize);
    protected String fontName = DEFAULT_FONT;
    protected int fontWeight = 0;
    protected int fontStyle = 0;
    protected int underLine = 0;
    protected int borderTop = 0;
    protected int borderBotom = 0;
    protected boolean bottomLine = false;
    protected String alignment = MyComponent.ALIGN_LEFT;
    protected float imageableHeight = 0;
    private float imageableWidth = 0;
    protected float length = 0;
    protected float widthPercentage = 100; // %
    protected float width;
    protected float imageableX;
    protected float imageableY;
    protected boolean hasBorder = false;
    protected float paddingRight = 0;
    protected float paddingLeft = 0;
    protected float paddingTop = 0;
    protected float paddingBottom = 0;
    protected final Border border = new Border();
    protected float yOffset = 0;
    protected double scale = 1.0;
    //
    // margins are culculated in percentage
    protected float leftMarginPercentage = 7f; // %    
    protected float topMarginPercentage = 7f; // %
    protected float rightMarginPercentage = 7f; // %
    protected float bottomMarginPercentage = 7f; // %
    protected float leftMargin = -1;
    protected float topMargin = -1;
    protected float rightMargin = -1;
    protected float bottomMargin = -1;
    protected float baseLine = 0;
    protected boolean pageBrake;
    public static float PAGE_HEIGHT = 0;
    public boolean fill = false;
    public Color fillColor = Color.BLACK;
    //
    protected MyComponent parent;
    protected MyComponent setting;
    protected List<MyComponent> components = new LinkedList<>();
    protected OriginStrategy origin;
    //public static boolean IS_RENDERING = false;
    private LineType borderLineType = LineType.SOLID;
    private boolean drawingCall = true; //set if we dont want a component to be drawn.
    private final ArrayList<Integer> componentVisiblePages = new ArrayList<>();

    private List<MyComponent> xComponents = new LinkedList<>();
    private List<MyComponent> yComponents = new LinkedList<>();
    private BufferedImage backGroungImage;
    private boolean isPercentageWidth = false;
    private float height;
    private String verticleAlignment;

    public MyComponent() {
        this.origin = OriginStrategy.TopLeftOrigin;
    }

    @Override
    protected T clone() {
        T cloned = null;
        try {
            cloned = (T) super.clone();
            cloned.setting = this.setting;
            cloned.components = this.getComponents();
            cloned.origin = this.getOrigin();
            cloned.parent = this.getParent();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(MyComponent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MyComponent.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cloned;
    }

    /**
     * draws component data to media
     *
     * @param renderer
     * @param imageableWidth
     * @param imageableHeight
     * @param imageableX
     * @param imageableY
     */
    protected abstract void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight);

    protected abstract float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight);

    public OriginStrategy getOrigin() {

        if (parent != null) {
            return parent.getOrigin();
        }

        return origin;
    }

    public T setOrigin(OriginStrategy origin) {
        this.origin = origin;
        return (T) this;
    }

    public double getScale() {
        return scale;
    }

    public T setScale(double scale) {
        this.scale = scale;
        return (T) this;
    }

    public boolean isPageBrake() {
        return pageBrake;
    }

    public T setPageBrake(boolean pageBrake) {
        this.pageBrake = pageBrake;
        return (T) this;
    }

    public T setData(Object text) {
        this.data = text;
        return (T) this;
    }

    public T setBaseline() {
        this.baseLine = 0;
        return (T) this;
    }

    public T renderComponent(Renderer renderer, float imageableWidth, float imageableHeigt, float imageableX, float imageableY) {
        drawData(renderer, imageableX, imageableY, imageableWidth, imageableHeigt);
        return (T) this;
    }

    public T setBorderOrigin(float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        border.setX(imageableX);
        border.setY(imageableY);
        border.setWIDTH(imageableWidth);
        border.setHEIGHT(imageableHeight);
        return (T) this;
    }

    public Border getBorder() {
        return border;
    }

    public T setBorderLineDouble(boolean type) {
        getBorder().setDrawDaubleLine(type);
        return (T) this;
    }

    public T setBorderColor(Color color) {
        getBorder().setColor(color);
        return (T) this;
    }

    public Color getBorderColor() {
        return getBorder().getColor();
    }

    public T setFill(boolean fill) {
        getBorder().setFill(fill);
        return (T) this;
    }

    public boolean hasAFill() {
        return getBorder().hasAfill();
    }

    public T setFillColor(Color fillColor) {
        getBorder().setFillColor(fillColor);
        return (T) this;
    }

    public Color getFillColor() {
        return getBorder().getFillColor();
    }

    public T setBorderLineType(LineType lineType) {
        this.borderLineType = lineType;
        return (T) this;
    }

    public LineType getBorderLineType() {
        return this.borderLineType;
    }

    public void resetLineType() {
        this.borderLineType = LineType.SOLID;
    }

    public T addComponent(MyComponent component) {
        this.components.add(component);
        component.setParent(this);
        component.afterAddEvent();

        return (T) this;
    }

    public T addYComponent(MyComponent component) {
        this.yComponents.add(component);
        imageableY += component.getLength();

        return (T) this;
    }

    public T addXComponent(MyComponent component) {
        this.xComponents.add(component);
        imageableX += component.getLength();

        return (T) this;
    }

    public T setComponents(ArrayList<MyComponent> components) {
        for (int i = 0; i < components.size(); i++) {
            MyComponent component = components.get(i);
            component.setParent(this);
            component.afterAddEvent();
            component.setChildrenDefaultFontSize(component.getFontSize());
        }
        this.components = components;
        return (T) this;
    }

    public T setFont(String fontName) {
        this.font = new Font(fontName, Font.PLAIN, this.getFontSize());
        return (T) this;
    }

    public T setFontName(String fontName) {
        this.fontName = fontName;
        return (T) this;
    }

    public String getFontName() {
        return fontName;
    }

    public T setFontSize(int num) {
        String currentFontName = this.font.getName();
        this.font = new Font(currentFontName, Font.PLAIN, (int) num);
        this.fontSize = num;
        return (T) this;
    }

    public T setAlignment(String align) {
        this.alignment = align;
        return (T) this;
    }

    public T setBottomMargin(float bottomMargin) {
        this.bottomMargin = bottomMargin;
        return (T) this;
    }

    public T setRightMargin(float rightMargin) {
        this.rightMargin = rightMargin;
        return (T) this;
    }

    public T setTopMargin(float topMargin) {
        this.topMargin = topMargin;
        return (T) this;
    }

    public T setLeftMargin(float leftMargin) {
        this.leftMargin = leftMargin;
        return (T) this;
    }

    public T setMargin(float margin) {
        setMargins(margin);
        return (T) this;
    }

    public T setWidthPercentage(float width) {
        this.widthPercentage = width;
        return (T) this;
    }

    public T setLeftMarginPercentage(float leftMarginPercentage) {
        this.leftMarginPercentage = leftMarginPercentage;
        return (T) this;
    }

    public T setTopMarginPercentage(float topMarginPercentage) {
        this.topMarginPercentage = topMarginPercentage;
        return (T) this;
    }

    public T setRightMarginPercentage(float rightMarginPercentage) {
        this.rightMarginPercentage = rightMarginPercentage;
        return (T) this;
    }

    public T setBottomMarginPercentage(float bottomMarginPercentage) {
        this.bottomMarginPercentage = bottomMarginPercentage;
        return (T) this;
    }

    public T setMarginPercents(float margin) {
        this.setLeftMarginPercentage(margin);
        this.setTopMarginPercentage(margin);
        this.setRightMarginPercentage(margin);
        this.setBottomMarginPercentage(margin);
        return (T) this;
    }

    public T addComponentVisiblePages(int pageNumber) {
        componentVisiblePages.add(pageNumber);
        return (T) this;
    }

    public ArrayList<Integer> getComponentVisiblePages() {
        return componentVisiblePages;
    }

    protected void setMargins(float margin) {
        this.setTopMargin(margin);
        this.setRightMargin(margin);
        this.setBottomMargin(margin);
        this.setLeftMargin(margin);
    }

    protected void setWidth(float width) {
        this.width = width;
    }

    /**
     * Height used internally for setting fill and border height on TableRow
     * when some cells height is taller than others
     *
     * @return
     */
    protected void setHeight(float height) {
        this.height = height;
    }

    /**
     * Height used internally for setting fill and border height on TableRow
     * when some cells height is taller than others
     *
     * @return
     */
    protected float getHeight() {
        return height;
    }

    public String getVerticleAlignment() {
        return verticleAlignment;
    }

    public T setVerticleAlignment(String verticleAlignment) {
        this.verticleAlignment = verticleAlignment;
        return (T) this;
    }

    public void setVerticleOffset(float dataLength) {
        float verticalOffset = 0;
        dataLength = dataLength + getPaddingTop() + getPaddingBottom()+(getBorder().getLength() *2);
        if (getHeight() > 0) {
            switch (getVerticleAlignment()) {
                case VERTICLE_ALIGN_CENTER:
                    verticalOffset = (getHeight() - dataLength) / 2;
                    break;
                case VERTICLE_ALIGN_BOTTOM:
                    verticalOffset = getHeight() - dataLength;
                    break;
                case VERTICLE_ALIGN_TOP:
                default:
                    verticalOffset = 0; // Top alignment as default
                    break;
            }

        }

                shiftY(verticalOffset);
    }

    protected void setImageableHeight(float height) {
        this.imageableHeight = height;
    }

    protected void setImageableWidth(float imageablewidth) {
        this.imageableWidth = imageablewidth;
    }

    public T setImageableX(float imageableX) {
        this.imageableX = imageableX;
        return (T) this;
    }

    public T setImageableY(float imageableY) {
        this.imageableY = imageableY;
        return (T) this;
    }

    public T setFontWeight(int fontWeight) {
        this.fontWeight = fontWeight;
        return (T) this;
    }

    /**
     * Sets the font style for a text element. This method allows setting
     * various font styles, including bold, italic, bold-italic, and underline.
     * The specific font styles are represented by predefined constants:
     * STYLE_BOLD, STYLE_ITALIC, STYLE_BOLD_ITALIC, and STYLE_UNDERLINE.
     *
     * @param fontStyle An integer representing the font style.
     */
    public T setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
        return (T) this;
    }

    public T setUnderLine(int underLine) {
        this.underLine = underLine;
        return (T) this;
    }

    public T setBorderBotom(int borderBotom) {
        this.borderBotom = borderBotom;
        return (T) this;
    }

    public T setBorderTop(int borderTop) {
        this.borderTop = borderTop;
        return (T) this;
    }

    protected T setLength(float length) {
        this.length = length;
        return (T) this;
    }

    public void increaseLength(float length) {
        setLength(getLength() + length);
    }

    public MyComponent getParent() {
        return this.parent;
    }

    public List<MyComponent> getComponents() {
        return this.components;
    }

    public float getWidthPercentage() {
        return this.widthPercentage;
    }

    public String getAlignment() {
        return alignment;
    }

    public int getFontSize() {
        return fontSize;
    }

    public Font getFont() {
        return font;
    }

    public Object getData() {
        return this.data;
    }

    public float getImageableHeight() {
        return this.imageableHeight;
    }

    public float getLeftMarginPercentage() {
        return leftMarginPercentage;
    }

    public float getTopMarginPercentage() {
        return topMarginPercentage;
    }

    public float getBottomMarginPercentage() {
        return bottomMarginPercentage;
    }

    public float getRightMarginPercentage() {
        return rightMarginPercentage;
    }

    public float getBaseLine() {
        return this.baseLine;
    }

    public void shiftY(float distance) {
        setImageableY(getOrigin().shiftImageableY(getImageableY(), distance));
    }

    public void shiftX(float distance) {
        setImageableX(getImageableX() + distance);
    }

    protected void checkPageBrake(float imageableHeight) {
        if (isPageBrake()) {
            setLength(imageableHeight);
            //increaseLength(imageableHeight - getLength());
        }
    }

    /**
     * mover y coordinate, increase component length and reduce component
     * imageable length at once.
     *
     * @param distance the amount of distance to translate.
     */
    public void translateHeight(float distance) {
        this.shiftY(distance);
        this.increaseLength(distance);
        this.setImageableHeight(this.getImageableHeight() - distance);
    }

    /**
     * move x coordinate, increase component witdh and reduce component
     * imageable width at once.
     *
     * @param distance the amount of distance to translate.
     */
    public void translateWidth(float distance) {
        this.shiftX(distance);
        this.setImageableWidth(this.getImageableWidth() - distance);
    }

    public void beforeDrawing(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        if (getWidth() < 1) {
            this.isPercentageWidth = true;
            this.setWidth(this.calculatePecentageSize(this.getWidthPercentage(), imageableWidth));
        }
        //set component margins
        setMargins();

        //remove margins from width and move pen to position
        setImegeables(imageableX, imageableY, imageableWidth, imageableHeight);
        //set border origin before removing padding. should we need to draw a border latter.
        setBorderOrigin(getImageableX(), getImageableY(), getImageableWidth(), 0);

        if (this.getBorderTop() == 1) {
            if (isDrawingCall()) {
                renderer.drawLine(getBorder());
            }
            translateHeight(getBorder().getLength());
        }

        setPaddings();

    }

    public void afterDrawing(Renderer renderer) {
        // consume bottom padding length
        translateHeight(getPaddingBottom());

        //draw bottom line
        if (this.getBorderBotom() == 1) {
            getBorder().setY(getImageableY());
            getBorder().setLineType(getBorderLineType());
            renderer.drawLine(getBorder());
            resetLineType();
            translateHeight(getBorder().getLength());
        }

        //consume bottom margin length
        translateHeight(getMarginBottom());

        if (this.isBottomLine()) {
            setLength(getLength() + getFontSize());
        }
    }

    public void drawBoarder(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        //draw boarder
        if (isHasBorder() || hasAFill()) {
            //backup height information before geting component height
            float imageableYOld = getImageableY();
            float imageableHeightOld = getImageableHeight();

            //get old drawing call
            boolean oldDrawingCall = isDrawingCall();

            //get table row height
            float tableRowHeight = drawData2(false, renderer, imageableX, imageableY, imageableWidth, imageableHeight);

            if (getHeight() > 0) {
                tableRowHeight = getHeight();
            }

            //reset to old drawing call;
            setDrawingCall(oldDrawingCall);

            getBorder().setHEIGHT(tableRowHeight - (getMarginTop() + getMarginBottom()));
            if (isDrawingCall()) {
                renderer.drawBorder(getBorder());
            }
            translateHeight(getBorder().getLength());
            translateHeight(getBorder().getLength());

            //move cursor back to backed up position.
            setImageableHeight(imageableHeightOld - tableRowHeight);
            setImageableY(imageableYOld);
        }
    }

    public float getWidth() {
        return width;
    }

    public float getLength() {
        return length;
    }

    public float getMarginLeft() {
        return leftMargin;
    }

    public float getMarginTop() {
        return topMargin;
    }

    public boolean isBottomLine() {
        return bottomLine;
    }

    public float getMarginRight() {
        return rightMargin;
    }

    public float getMarginBottom() {
        return bottomMargin;
    }

    public float getImageableX() {
        return imageableX;
    }

    public float getImageableY() {
        return imageableY;
    }

    public float getImageableWidth() {
        return this.imageableWidth;
    }

    public int getFontWeight() {
        return fontWeight;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public int getUnderLine() {
        return underLine;
    }

    public int getBorderTop() {
        return borderTop;
    }

    public int getBorderBotom() {
        return borderBotom;
    }

    public MyComponent getComponentAt(int index) {
        return this.getComponents().get(index);
    }

    protected void setParent(MyComponent parent) {
        this.parent = parent;
    }

    public T setBottomLine(boolean bottomLine) {
        this.bottomLine = bottomLine;
        return (T) this;
    }

    protected void afterAddEvent() {
        this.runSettables();
    }

    public String setDefaultAlignment() {
        String result = MyComponent.ALIGN_LEFT;
        if (this.getAlignment().equals(MyComponent.ALIGN_LEFT)) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultAlignment();
            }
            this.setAlignment(result);
        }
        return this.getAlignment();
    }

    public String setDefaultVerticleAlignment() {
        String result = MyComponent.VERTICLE_ALIGN_TOP;
        if (this.getVerticleAlignment() == null || this.getVerticleAlignment().equals(MyComponent.VERTICLE_ALIGN_TOP)) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultVerticleAlignment();
            }
            this.setVerticleAlignment(result);
        }
        return this.getVerticleAlignment();
    }

    protected String setDefaultFont() {
        String result = MyComponent.DEFAULT_FONT;
        if (this.getFont().getName().equals(MyComponent.DEFAULT_FONT)
                && this.getFont().getSize() == MyComponent.DEFAULT_FONT_SIZE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultFont();
            }
            this.setFont(result);
        }
        return this.getFont().getName();
    }

    protected int setDefaultFontSize() {
        int result = MyComponent.DEFAULT_FONT_SIZE;
        if (this.getFontSize() == MyComponent.DEFAULT_FONT_SIZE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultFontSize();
            }
            this.setFontSize(result);
        }
        return this.getFontSize();
    }

    protected void setChildrenDefaultFontSize(int fontSize) {
        for (int i = 0; i < this.getComponents().size(); i++) {
            MyComponent c = getComponents().get(i);
            if (c.getFontSize() == MyComponent.DEFAULT_FONT_SIZE) {
                c.setChildrenDefaultFontSize(fontSize);
                c.setFontSize(fontSize);
            }
        }
    }

    public float setDefaultWidthPercent() {
        float result = MyComponent.DEFAULT_WIDTH_PERCENTAGE;
        if (this.getWidthPercentage() == MyComponent.DEFAULT_WIDTH_PERCENTAGE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultWidthPercent();
            }
            this.setWidthPercentage(result);
        }
        return this.getWidthPercentage();
    }

    public float setDefaultMargins() {
        float result = MyComponent.DEFAULT_LEFT_MARGIN_PERCENTAGE;
        if (this.getLeftMarginPercentage() == MyComponent.DEFAULT_LEFT_MARGIN_PERCENTAGE
                && this.getTopMarginPercentage() == MyComponent.DEFAULT_TOP_MARGIN_PERCENTAGE
                && this.getRightMarginPercentage() == MyComponent.DEFAULT_RGIHT_MARGIN_PERCENTAGE
                && this.getBottomMarginPercentage() == MyComponent.DEFAULT_BOTTOM_MARGIN_PERCENTAGE) {
            if (this.getParent() != null) {
                result = this.getParent().setDefaultMargins();
            }
            this.setMarginPercents(result);
        }
        return this.getLeftMarginPercentage();
    }

    protected void runSettables() {
        this.setDefaultAlignment();
        this.setDefaultFont();
        this.setDefaultFontSize();
        this.setDefaultVerticleAlignment();
        this.setDefaultMargins();

        //this.setDefaultWidthPercent();
    }

    public float alignCenter(float leftMargin, float textWidth) {
        float result = 0;
        result = (this.getWidth() - textWidth);

        if (result > 0) {
            result = result / 2;
        } else {
            result = 0;
        }
        return result + leftMargin;
    }

    public float alignRight(float leftMargin, float textWidth) {
        float result = 0;
        result = (this.getWidth() - textWidth);
        return result + leftMargin;
    }

    protected float calculatePecentageSize(float pecentage, float parentWidth) {
        float result = 0;
        result = pecentage * parentWidth / 100;
        return result;

    }

    protected void setMargins() {
        if (getMarginLeft() < 0) { //if user set margins stright other than marginpercentage, then dont use margin percentage.
            this.setLeftMargin(this.calculatePecentageSize(this.getLeftMarginPercentage(), this.getWidth()));
        }

        if (getMarginTop() < 0) {
            this.setTopMargin(this.calculatePecentageSize(this.getTopMarginPercentage(), this.getWidth()));
        }
        if (getMarginRight() < 0) {
            this.setRightMargin(this.calculatePecentageSize(this.getRightMarginPercentage(), this.getWidth()));
        }

        if (getMarginBottom() < 0) {
            this.setBottomMargin(this.calculatePecentageSize(this.getBottomMarginPercentage(), this.getWidth()));
        }
    }

    protected void setImegeables(float imageablex, float imageableY, float imageableWidth, float imageableHeight) {
        try {

            float k = getOrigin().setImageableY(imageableY, imageableHeight);

            //shift imageables
            this.setImageableX(imageablex + this.getMarginLeft());
            this.setImageableY(getOrigin().shiftImageableY(k, getMarginTop()));

            this.setImageableHeight(imageableHeight - getMarginTop());

            this.setImageableWidth(getWidth() - getMarginLeft() - getMarginRight());
            //set initial length to what we have consumed so far. "marginTop, marginBottom"
            setLength(getMarginTop());

        } catch (Exception ex) {
            Logger.getLogger(MyComponent.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void setPaddings() {
        //set paddings
        setImageableWidth(getImageableWidth() - getPaddingLeft() - getPaddingRight());
        setImageableHeight(getImageableHeight() - getPaddingTop());

        shiftX(getPaddingLeft());
        shiftY(getPaddingTop());

        //set the length we have consumed so far
        increaseLength(getPaddingTop());
    }

    protected void drawDebugingBorder(Graphics2D canvas) {
        canvas.drawRect((int) getImageableX(), (int) getImageableY(), (int) this.getImageableWidth(), (int) this.getImageableHeight());
    }

    public T setPaddingRight(float padding) {
        this.paddingRight = padding;
        return (T) this;
    }

    public float getPaddingRight() {
        return paddingRight;
    }

    public T setPaddingLeft(float padding) {
        this.paddingLeft = padding;
        return (T) this;
    }

    public float getPaddingLeft() {
        return paddingLeft;
    }

    public T setPaddingTop(float padding) {
        this.paddingTop = padding;
        return (T) this;
    }

    public float getPaddingTop() {
        return paddingTop;
    }

    public T setPaddingBottom(float padding) {
        this.paddingBottom = padding;
        return (T) this;
    }

    public float getPaddingBottom() {
        return paddingBottom;
    }

    public boolean isHasBorder() {
        return getBorder().canDrawBoarder();
    }

    public boolean hasBackGround() {
        return getBackGroundImage() != null;
    }

    public T setBackGroundImage(BufferedImage backGroungImage) {
        this.backGroungImage = backGroungImage;
        return (T) this;
    }

    public BufferedImage getBackGroundImage() {
        return this.backGroungImage;
    }

    public T setBorder(boolean hasBorder) {
        getBorder().setDrawBoarder(hasBorder);
        return (T) this;
    }

    public T setPadding(float padding) {
        setPaddingBottom(padding);
        setPaddingLeft(padding);
        setPaddingRight(padding);
        setPaddingTop(padding);
        return (T) this;
    }

    public T setDrawDaubleLine(boolean state) {
        getBorder().setDrawDaubleLine(state);
        return (T) this;
    }

    /**
     *
     * @param depth
     * @return
     */
    public T setBorderLineDepth(float depth) {
        getBorder().setLineDepth(depth);
        return (T) this;
    }

    public T setBoarderDoubleLineDepth(float line1depth, float line2depth) {
        getBorder().setLineDepth(line1depth);
        getBorder().setLine2Depth(line2depth);
        return (T) this;
    }

    public T setBoarderDoubleSpacing(float spacing) {
        getBorder().setDoubleLineStrut(spacing);
        return (T) this;
    }

    public T setYOffSet(float yOffset) {
        this.yOffset = yOffset;
        return (T) this;
    }

    float getYOffest() {
        return yOffset;
    }

    /**
     * Determines if canvas should or should not draw this component. Useful if
     * all is need is to determine the height of this component and not to draw
     * it.
     *
     * @return
     */
    public boolean isDrawingCall() {
        return drawingCall;
    }

    public T setDrawingCall(boolean drawingCall) {
        if (getComponents().size() > 0) {
            for (int i = 0; i < getComponents().size(); i++) {
                MyComponent get = getComponents().get(i);
                get.setDrawingCall(drawingCall);
            }
        }

        this.drawingCall = drawingCall;
        return (T) this;

    }

    public class Border {

        private float X = 0, Y = 0, WIDTH = 0, HEIGHT = 0;
        private LineType lineType = LineType.SOLID;
        private float lineDepth = (float) 1;
        private float vissibleDashWidth = 2;
        private float inVissibleDashWidth = (float) 1.5;
        private boolean drawDaubleLine = false;
        private float doubleLineStrut = lineDepth * 2;
        private Color color = Color.BLACK;
        private float line2Depth = (float) 1;
        private boolean fill = false;
        private Color fillColor = Color.BLACK;
        private boolean drawBoarder = false;
        private boolean drawBackGround = false;
        private BufferedImage backGroungImage;

        protected void draw(Graphics2D canvas, float imageableX, float imageableY, float imageableWidth, float imageableHeigt) {

            canvas.setPaint(getColor());
            canvas.setStroke(getStroke());
            canvas.drawRect((int) imageableX, (int) imageableY, (int) imageableWidth, (int) imageableHeigt);
            canvas.setPaint(Color.BLACK);
        }

        protected void draw(Graphics2D canvas) {
            draw(canvas, getX(), getY(), getWIDTH(), getHEIGHT());

            if (isDrawDaubleLine()) {
                float name = 2 * (getDoubleLineStrut() + getLineDepth());
                draw(canvas, getX() + getDoubleLineStrut(), getY() + getDoubleLineStrut(), getWIDTH() - name, getHEIGHT() - name);
            }
        }

        public void drawLine(Graphics2D canvas) {
            canvas.setStroke(getStroke());
            canvas.drawLine((int) getX(), (int) getY(), (int) getWIDTH() + (int) getX(), (int) getY());
            if (isDrawDaubleLine()) {
                float name = getDoubleLineStrut();
                canvas.drawLine((int) getX(), (int) getY() + (int) name, (int) getWIDTH() + (int) getX(), (int) getY() + (int) name);
            }
        }

        protected void drawPDFBorder(PDFCanvas pDFCanvas, float imageableX, float imageableY, float imageableWidth, float imageableHeigt) throws IOException {
            pDFCanvas.getPageContentStream().addRect(imageableX, imageableY - imageableHeigt, imageableWidth, imageableHeigt);
            pDFCanvas.getPageContentStream().stroke();

        }

        protected void drawPDFBorder(PDFCanvas pDFCanvas) throws IOException {
            drawPDFBorder(pDFCanvas, getX(), getY(), getWIDTH(), getHEIGHT());
            if (isDrawDaubleLine()) {
                float name = 2 * (getDoubleLineStrut() + getLineDepth());
                drawPDFBorder(pDFCanvas, getX() + getDoubleLineStrut(), getY() + getDoubleLineStrut(), getWIDTH() - name, getHEIGHT() - name);
            }
        }

        public void drawPDFLine(PDFCanvas pdfCanvas) throws IOException {
            PDPageContentStream contents = pdfCanvas.getPageContentStream();
            contents.moveTo(getX(), getY());
            contents.lineTo(getWIDTH() + getX(), getY());
            contents.stroke();
            if (isDrawDaubleLine()) {
                float name = getDoubleLineStrut();
                contents.moveTo(getX(), getY() + name);
                contents.lineTo(getWIDTH() + getX(), getY() + name);
                contents.stroke();
            }
        }

        public float getLength() {
            float l = getLineDepth();
            if (isDrawDaubleLine()) {
                l = getLineDepth() +getDoubleLineStrut() + getLineDepth();
            }
            return l;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        public float getX() {
            return X;
        }

        public void setX(float X) {
            this.X = X;
        }

        public float getY() {
            return Y;
        }

        public void setY(float Y) {
            this.Y = Y;
        }

        public float getWIDTH() {
            return WIDTH;
        }

        public void setWIDTH(float WIDTH) {
            this.WIDTH = WIDTH;
        }

        public float getHEIGHT() {
            return HEIGHT;
        }

        public void setHEIGHT(float HEIGHT) {
            this.HEIGHT = HEIGHT;
        }

        private LineType getLineType() {
            return lineType;
        }

        public void setLineType(LineType lineType) {
            this.lineType = lineType;
        }

        public float getLineDepth() {
            return lineDepth;
        }

        public void setLineDepth(float lineDepth) {
            this.lineDepth = lineDepth;
        }

        public float getLine2Depth() {
            return line2Depth;
        }

        public void setLine2Depth(float lineDepth) {
            this.line2Depth = lineDepth;
        }

        public float getVissibleDashWidth() {
            return vissibleDashWidth;
        }

        public void setVissibleDashWidth(float vissibleDashWidth) {
            this.vissibleDashWidth = vissibleDashWidth;
        }

        public float getInVissibleDashWidth() {
            return inVissibleDashWidth;
        }

        public void setInVissibleDashWidth(float inVissibleDashWidth) {
            this.inVissibleDashWidth = inVissibleDashWidth;
        }

        public boolean isDrawDaubleLine() {
            return drawDaubleLine;
        }

        public void setDrawDaubleLine(boolean drawDaubleLine) {
            this.drawDaubleLine = drawDaubleLine;
        }

        public float getDoubleLineStrut() {
            return doubleLineStrut;
        }

        public void setDoubleLineStrut(float doubleLineStrut) {
            this.doubleLineStrut = doubleLineStrut;
        }

        public Stroke getStroke() {
            return getStroke(DoubleBoarderType.OUTER);
        }

        public Stroke getStroke(DoubleBoarderType boarderIndex) {
            float linedepth = this.getLineDepth();
            if (boarderIndex == DoubleBoarderType.INNER) {
                linedepth = this.getLine2Depth();
            }
            Stroke stroke = new BasicStroke(linedepth);
            if (getLineType() == LineType.DASHED) {
                stroke = new BasicStroke(linedepth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{this.vissibleDashWidth, this.inVissibleDashWidth}, 0);
            }
            return stroke;
        }

        public void setFill(boolean fill) {
            this.fill = fill;
        }

        public boolean hasAfill() {
            return fill;
        }

        public void setFillColor(Color fillColor) {
            this.fillColor = fillColor;
        }

        public Color getFillColor() {
            return fillColor;
        }

        public boolean canDrawBoarder() {
            return drawBoarder;
        }

        public void setDrawBoarder(boolean drawBoarder) {
            this.drawBoarder = drawBoarder;
        }

        public void setBackGroundImage(BufferedImage backGroungImage) {
            this.backGroungImage = backGroungImage;
        }

        public BufferedImage getBackgroundImage() {
            return this.backGroungImage;
        }
    }

    public enum DoubleBoarderType {
        INNER,
        OUTER
    }

    public static class PDFCanvas {

        private PDDocument pdfDoc;
        private PDPage pdfPage;
        private PDPageContentStream pageContentStream = null;

        public PDFCanvas(PDDocument pdfDoc, PDPage blankPage) {
            try {
                this.pdfDoc = pdfDoc;
                this.pdfPage = blankPage;
                this.pageContentStream = new PDPageContentStream(pdfDoc, pdfPage, PDPageContentStream.AppendMode.APPEND, true);
            } catch (IOException ex) {
                Logger.getLogger(MyComponent.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public PDDocument getPdfDoc() {
            return pdfDoc;
        }

        public void setPdfDoc(PDDocument pdfDoc) {
            this.pdfDoc = pdfDoc;
        }

        public PDPage getPdfPage() {
            return pdfPage;
        }

        public void setPdfPage(PDPage pdfPage) {
            this.pdfPage = pdfPage;
        }

        public PDPageContentStream getPageContentStream() {
            return pageContentStream;
        }

        public void setPageContentStream(PDPageContentStream pageContentStream) {
            this.pageContentStream = pageContentStream;
        }

        public void dispose() {
            if (pageContentStream != null) {
                try {
                    pageContentStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(MyComponent.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
}
