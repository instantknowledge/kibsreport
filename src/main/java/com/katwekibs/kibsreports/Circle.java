/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;

/**
 *
 * @author muhamedkakembo
 */
public class Circle extends MyComponent<Circle> {

    private float boxWidth = 28.3465F;
    private float boxHeight = 28.3465F;

    public Circle() {
        setMarginPercents(0);
    }

    public Circle(String data) {
        setData(data);
        setMarginPercents(0);
    }

    public static Circle Paragraph(String data) {
        return new Circle().setData(data);
    }

    public float getBoxWidth() {
        return boxWidth;
    }

    public Circle setBoxWidth(float boxWidth) {
        this.boxWidth = boxWidth;
        setWidth(boxWidth);
        return this;
    }

    public float getBoxHeight() {
        return boxHeight;
    }

    public Circle setBoxHeight(float height) {
        this.boxHeight = height;
        return this;
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);

        renderer.drawCircle(this);

        afterDrawing(renderer);
        return getLength();
    }

}
