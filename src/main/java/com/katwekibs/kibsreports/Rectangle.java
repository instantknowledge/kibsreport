/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;
import java.awt.Color;

/**
 *
 * @author muhamedkakembo
 */
public class Rectangle extends MyComponent<Rectangle> {
   private float boxWidth = 20;
   private float boxHeight = 20;
    private Color fillColor = Color.BLACK;

    public Rectangle() {
        setMarginPercents(0);
    }

    public Rectangle(String data) {
        setData(data);
        setMarginPercents(0);
    }

    public static Rectangle Rectangle() {
        return new Rectangle().setData(null);
    }
    public static Rectangle Rectangle(String data) {
        return new Rectangle().setData(data);
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
                drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    public float getBoxWidth() {
        return boxWidth;
    }

    public Rectangle setBoxWidth(float boxWidth) {
        this.boxWidth = boxWidth;
                setWidth(boxWidth);
        return this;
    }

    public float getBoxHeight() {
        return boxHeight;
    }

    public Rectangle setBoxHeight(float height) {
        this.boxHeight = height;
        return this;
    }

    public Rectangle setFillColor(Color color) {
        fillColor = color;
        return this;
    }

    public Color getFillColor() {
        return fillColor;
    } 

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        
        renderer.drawRectangle(this);
        
        afterDrawing(renderer);
        return getLength();
    }
    
    
}
