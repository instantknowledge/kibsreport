/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author muhamedkakembo
 */
public class Table extends MyComponent<Table> {

    private List<MyComponent> columns = new LinkedList<>();
    private MyComponent columnHeaderRow;
    private List<MyComponent> bodyRaws = new LinkedList<>();
    private float columnWidthPercentage;
    private BaseTableModel model;

    public Table() {
        setMarginPercents(0);
        this.columnHeaderRow = new TableRow();
        columnHeaderRow.setParent(this);
    }

    public static Table Table() {
        return new Table();
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    protected void drawBorder(Graphics2D canvas, float imageableWidth, float imageableHeigt, float imageableX, float imageableY) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Table setModel(BaseTableModel model) {
        this.model = model;
        this.setTableValues();
        columnHeaderRow.setDefaultFont();
        columnHeaderRow.setDefaultFontSize();
        return this;
    }

    public Table setDefaultColumnWidtPercentage(int length) {
        this.columnWidthPercentage = 100 / length;
        return this;
    }

    public MyComponent getColumn(int index) {
        return this.columns.get(index);
    }

    public int getColumnCount() {
        return this.columns.size();
    }

    public MyComponent getColumnHeader() {
        return this.columnHeaderRow;
    }

    public BaseTableModel getModel() {
        return model;
    }

    private void setColumNames() {
        if (model.getColumnCount() > 0) {
            this.setDefaultColumnWidtPercentage(model.getColumnCount());
            for (int i = 0; i < model.getColumnCount(); i++) {
                MyComponent paragraph = new Paragraph();
                paragraph.setData(model.getColumnName(i));
                paragraph.setWidthPercentage(this.columnWidthPercentage);
                this.columns.add(paragraph);
            }
        }
    }

    private void setColumnHeader() {
        if (this.getColumnCount() > 0) {
            for (int i = 0; i < this.getColumnCount(); i++) {
                columnHeaderRow.addComponent(this.getColumn(i));
            }
        }
    }

    private void setBodyRows() {
        if (this.model.getRowCount() > 0) {
            for (int i = 0; i < model.getRowCount(); i++) {
                MyComponent row = new TableRow();
                if (this.getColumnCount() > 0) {
                    for (int column = 0; column < this.getColumnCount(); column++) {
                        MyComponent paragraph = new Paragraph();
                        paragraph.setData(this.getModel().getValueAt(i, column));
                        paragraph.setWidthPercentage(getColumn(column).getWidthPercentage());
                        paragraph.setFont(this.getFont().getName());
                        paragraph.setFontSize(this.getFontSize());
                        row.addComponent(paragraph);
                    }
                }
                this.bodyRaws.add(row);
            }
        }
    }

    private void setTableValues() {
        this.setColumNames();
        this.setColumnHeader();
        this.setBodyRows();
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {

        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);

        if (this.columnHeaderRow != null && this.columnHeaderRow.getComponents().size() > 0) {
            columnHeaderRow.drawData(renderer, this.getImageableX(), this.getImageableY(), this.getImageableWidth(), this.getImageableHeight());
            translateHeight(columnHeaderRow.getLength());
        }
        if (this.bodyRaws.size() > 0) {
            for (int i = 0; i < this.bodyRaws.size(); i++) {
                MyComponent row = this.bodyRaws.get(i);
                // make each rows alignment smilar to column headers alignment
                for (int j = 0; j < row.getComponents().size(); j++) {
                    MyComponent c = row.getComponentAt(j);
                    c.setAlignment(this.getColumnHeader().getComponentAt(j).getAlignment());
                }
                row.drawData(renderer, this.getImageableX(), this.getImageableY(), this.getImageableWidth(), this.getImageableHeight());
                translateHeight(row.getLength());
            }
        }

        afterDrawing(renderer);
        return getLength();
    }
}
