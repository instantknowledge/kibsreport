/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.katwekibs.kibsreports;

import com.katwekibs.kibsreports.strategies.renderStrategy.Renderer;

/**
 *
 * @author muhamedkakembo
 */
public class Paragraph extends MyComponent<Paragraph> {

    public Paragraph() {
        setMarginPercents(0);
    }

    public Paragraph(String data) {
        setData(data);
        setMarginPercents(0);
    }

    public static Paragraph Paragraph(String data) {
        return new Paragraph().setData(data);
    }

    @Override
    protected void drawData(Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        drawBoarder(renderer, imageableX, imageableY, imageableWidth, imageableHeight);
        drawData2(isDrawingCall(), renderer, imageableX, imageableY, imageableWidth, imageableHeight);
    }

    @Override
    protected float drawData2(boolean drawingCall, Renderer renderer, float imageableX, float imageableY, float imageableWidth, float imageableHeight) {
        beforeDrawing(renderer, imageableX, imageableY, imageableWidth, imageableHeight);

        renderer.drawText(this);

        afterDrawing(renderer);
        return getLength();
    }

}
